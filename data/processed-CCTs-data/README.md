This directory is for processed CCTs data (which can be too large to fit the default Docker container size).

You can download precomputed data from [Processed CCTs and FNs](https://my.pcloud.com/publink/show?code=kZLCfBkZHTAaOQDM5R4KLtoThSUkcQqFdSVX#folder=5025954764&tpl=publicfoldergrid)