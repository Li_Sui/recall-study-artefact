This directory is for Doop's results(super-setup) data (which can be too large to fit the default Docker container size).

You can download precomputed data from [Doop Static Call Graph](https://my.pcloud.com/publink/show?code=kZLCfBkZHTAaOQDM5R4KLtoThSUkcQqFdSVX#folder=5025954764&tpl=publicfoldergrid).