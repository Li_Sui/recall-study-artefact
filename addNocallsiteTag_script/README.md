### addNocallsiteTag.sh

addNocallsiteTag.sh script will do a post analysis on processed CCTs, add nocallsite tag to CCTs based on provided invocation lists. The results produced will be used for cause analysis

Required resources: processed CCTs are presented in data/processed-CCTs-data/, FNs are generated, invocation lists:data/processed-CCTs-data/program-invocationList.csv|library-invocationList.csv|jdk1.8.0_144-invocationList.csv

Required process: preAnalysis, unreflect-tests, runTest, processCCTs, runDoop, findFNs

output: overwrite CCTs to data/processed-CCTs-data/

- build for ALL programs `./addNocallsiteTag_script/addNocallsiteTag.sh` 
- build for ONE program(test program) `./addNocallsiteTag_script/addNocallsiteTag.sh _test` You can check out list of programs in property.sh
