#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#build all programs.
build_all(){
	echo "[recall-artefact]******start adding tag to all programs******"
	for program in "${programs[@]}" 
	do
		build_single "$program"
	done
}

#build one program
#$1=program
build_single(){
	echo "[recall-artefact]----------add tag to program $1-------------"
	add_nocallsiteTag "$1"
	echo "[recall-artefact]------------------------------------------"
}

#add tag to CCTs
#$1=programName,
add_nocallsiteTag(){
  check_prerequisite "$1"
  #create one if output folder is not exist
  mkdir -p "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"
  #analyse xcorpus to extract all callsite used for tagging CCT: nosuchcallsite
  #resolve dependencies, unzip drivers
  eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/exercise.xml compile  > /dev/null 2>&1"
  mkdir -p "${XCORPUS_DATA_DIR}"/"$1"/.xcorpus/build/builtinTestDriver
  mkdir -p "${XCORPUS_DATA_DIR}"/"$1"/.xcorpus/build/generatedTestDriver
  unzip -q "${XCORPUS_DATA_DIR}"/"$1"/.xcorpus/drivers-builtin-tests.zip -d "${XCORPUS_DATA_DIR}"/"$1"/.xcorpus/build/builtinTestDriver
  unzip -q "${XCORPUS_DATA_DIR}"/"$1"/.xcorpus/drivers-generated-tests.zip -d "${XCORPUS_DATA_DIR}"/"$1"/.xcorpus/build/generatedTestDriver
  echo "[recall-artefact] extract all invocations for detecting nosuchcallsite in CCTs "
  eval "java -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.XcorpusJarAnalyser ${XCORPUS_DATA_DIR}/$1 ${JAVA_HOME}/jre/lib/rt.jar ${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1"

  echo "[recall-artefact] adding nosuchcallsite tag to CCTs:,java parameters are set to -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize}"
  #you can change java parameters in property.sh
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.AddnocallsiteTagToCCTs -programDir ${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1 -outputDir ${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1"

}

check_prerequisite(){
  if [[ ! -d "${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1" ]] || [[ ! -f "${XCORPUS_DATA_DIR}/$1/.xcorpus/drivers-builtin-tests.zip" ]] || [[ ! -d "${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1/FNs-generatedTest" ]]
  then
      echo "[recall-artefact] ${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1 must exist! FNs must be generated, unreflect tests must be generated "
      exit 1
  fi
}

#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
