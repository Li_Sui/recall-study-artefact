#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#build all programs.
build_all(){
	echo "[recall-artefact]******start unreflecting tests for all programs******"
	for program in "${programs[@]}" 
	do
		build_single "$program"
	done
}

#build one program
#$1=program
build_single(){
	echo "[recall-artefact]----------unreflecting tests for program $1-------------"
	generate_drivers "$1"
}

#compile the program, generate/compile drivers for both builtin-tests and evosuite-tests
#$1=programName, all tasks are defined ant_unreflecteTest.xml
generate_drivers(){
 cp "${RECALL_ROOT_DIR}"/unreflectTests_script/ant_unreflectTest.xml "${RECALL_ROOT_DIR}"/xcorpus/tools
 echo "[recall-artefact] generating drivers for evosuite-tests"
 #compile/extract envosuite-tests
 eval "ant -f ${RECALL_ROOT_DIR}/xcorpus/tools/ant_unreflectTest.xml compile-generated-tests -Dprogram=$1 > /dev/null 2>&1"
 #generate drivers for evosuite-tests
 eval "ant -f ${RECALL_ROOT_DIR}/xcorpus/tools/ant_unreflectTest.xml generate-drivers-evosuite -Dprogram=$1"

 echo "[recall-artefact]generating drivers for builtin-tests"
 #compile/extract builtin-tests
 eval "ant -f ${RECALL_ROOT_DIR}/xcorpus/tools/ant_unreflectTest.xml compile-builtin-tests -Dprogram=$1 > /dev/null 2>&1"
 #generate drivers for builtin-tests
 eval "ant -f ${RECALL_ROOT_DIR}/xcorpus/tools/ant_unreflectTest.xml generate-drivers-builtin -Dprogram=$1"
}

#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
