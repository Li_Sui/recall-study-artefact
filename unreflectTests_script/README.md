### unreflect-tests.sh

unreflect-tests.sh script generates drivers for both builtin-tests and evosuite-tests.

Required resources: xcorpus/

Required process: none

output: compiled + source code drivers, two entry points for builtin-tests and evosuite-tests. You can find them in xcorpus/data/qualitas_corpus_20130901/${program}/.xcorpus/drivers-\*.zip

- build unreflect-tests(drivers) for ALL programs `./unreflectTest_script/unreflect-tests.sh` 
- build unreflect-tests(drivers) for ONE program(test program) `./unreflectTest_script/unreflect-tests.sh _test` You can check out list of programs in property.sh
