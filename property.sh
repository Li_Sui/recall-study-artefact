#!/bin/bash

#property file for list of programs
XCORPUS_DATA_DIR="${RECALL_ROOT_DIR}"/xcorpus/data/qualitas_corpus_20130901
programs=("ApacheJMeter_core-3.1" "castor-1.3.1" "checkstyle-5.1" "commons-collections-3.2.1" "drools-7.0.0.Beta6" "findbugs-1.3.9" "fitjava-1.1" "guava-21.0" "htmlunit-2.8" "informa-0.7.0-alpha2" "javacc-5.0" "jena-2.6.3" "jFin_DateMath-R1.0.1" "jfreechart-1.0.13" "jgrapht-0.8.1" "jrat-0.6" "jrefactory-2.9.19" "log4j-1.2.16" "lucene-4.3.0" "marauroa-3.8.1" "mockito-core-2.7.17" "nekohtml-1.9.14" "openjms-0.7.7-beta-1" "oscache-2.4.1" "pmd-4.2.5" "quartz-1.8.3" "tomcat-7.0.2" "trove-2.1.0" "velocity-1.6.4" "wct-1.5.2" "weka-3-7-9" )
#for processing raw CCTs and add nosuchcallsite tag e.g.  -Xmx64g -Xss512m
rawCCTheapMem=64g
rawCCTStackSize=512m
DOOP_TIME_OUT="--timeout 360"
