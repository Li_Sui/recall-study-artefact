### runTests.sh

runTests.sh instruments all classes, runs tests, logs method stacks

Required resources: 

- xcorpus/
- unreflect tests are produced: xcorpus/data/qualitas_corpus_20130901/${program}/.xcorpus/drivers-\*.zip
- xcorpus/data/qualitas_corpus_20130901/${program}/.xcorpus/nativeCallsite.txt has been generated

Required process: preAnalysis, unreflect-tests

output: raw CCTs produced to data/raw-CCTs-data/

- running for ALL programs `./runTest_script/runTests.sh`
- running for ONE program(test program) `./runTest_script/runTests.sh _test` You can check out list of programs in property.sh
