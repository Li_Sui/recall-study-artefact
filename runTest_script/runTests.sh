#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#run all programs.
build_all(){
	echo "[recall-artefact]******start runnning all programs******"
	for program in "${programs[@]}" 
	do
		build_single "${program}"
	done
}

#run one program
#$1=program
build_single(){
  echo "[recall-artefact]----------running program $1-------------"
  running_program "$1"
}

#compile the program, unzip drivers
#$1=programName, all tasks are defined ant_dcg.xml
running_program(){
  check_prerequisite "$1"
  #copy the ant build script to the program
  eval "cp ${RECALL_ROOT_DIR}/runTest_script/ant_dcg.xml ${XCORPUS_DATA_DIR}/$1/.xcorpus"
  #compile
  eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/exercise.xml compile-builtin-tests > /dev/null 2>&1"
  #run the ant target to run tests
  eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/ant_dcg.xml dcg-builtinTest"

  # after finfished, sleep for 2mins
  echo "[recall-artefact] sleeping for 2mins.... waiting files to be writen to disk"
  sleep 2m
  eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/exercise.xml compile-generated-tests > /dev/null 2>&1"
  #run the ant target to run tests
  eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/ant_dcg.xml dcg-generatedTest"
  #after finfished, sleep for 2mins
  echo "[recall-artefact] sleeping for 2mins.... waiting files to be writen to disk"
  sleep 2m
}

check_prerequisite(){
  if [[ ! -f "${XCORPUS_DATA_DIR}/$1/.xcorpus/drivers-builtin-tests.zip" ]] || [[ ! -f "${XCORPUS_DATA_DIR}/$1/.xcorpus/drivers-generated-tests.zip" ]] || [[ ! -f "${XCORPUS_DATA_DIR}/$1/.xcorpus/nativeCallsite.txt" ]]
  then
      echo "[recall-artefact] drivers-builtin-tests.zip, drivers-generated-tests.zip and nativeCallsite.txt must exist! Please run preAnalysis.sh and unreflect-tests.sh first "
      exit 1
  fi
}


#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
