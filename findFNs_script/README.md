### findFNs.sh

findFNs.sh script take SCG reachables and CCTs as input to identify FNs

Required resources: processed CCTs are presented to data/processed-CCTs-data/

Required process: preAnalysis, unreflect-tests, runTest, processCCTs, runDoop

output: write FNs to data/processed-CCTs-data/

- build for ALL programs `./findFNs_script/findFNs.sh` 
- build for ONE program(test program)  `./findFNs_script/findFNs.sh _test` You can check out list of programs in property.sh
