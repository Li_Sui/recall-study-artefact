#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#build all programs.
build_all(){
	echo "[recall-artefact]******start finding FNs for all programs******"
	for program in "${programs[@]}" 
	do
		build_single "$program"
	done
}

#build one program
#$1=program
build_single(){
	echo "[recall-artefact]----------finding FNs for program $1-------------"
	find_FNs "$1"
}

#find FNs
#$1=programName,
find_FNs(){
  check_prerequisite "$1"
  libResultDir="${RECALL_ROOT_DIR}/data/doop-results/lib-setup/$1"
  superResultDir="${RECALL_ROOT_DIR}/data/doop-results/super-setup/$1"
  #create output dir
  mkdir -p "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/FNs-builtinTest
  mkdir -p "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/FNs-generatedTest
  eval "java -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.ProduceFNS -recordings ${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1 -doopLib ${libResultDir} -doopSuper ${superResultDir}"
}

#$1=program
check_prerequisite(){
  if [[ ! -d "${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1" ]] || [[ ! -d "${RECALL_ROOT_DIR}/data/doop-results/lib-setup/$1" ]] || [[ ! -d "${RECALL_ROOT_DIR}/data/doop-results/super-setup/$1" ]]
  then
      echo "[recall-artefact] ${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1 must exist!, doop results must be generated "
      exit 1
  fi
}

#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
