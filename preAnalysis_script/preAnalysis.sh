#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#run all programs.
build_all(){
	echo "[recall-artefact]******start pre-analysing all programs******"
	for program in "${programs[@]}" 
	do
		build_single "$program"
	done
}

#run one program
#$1=program
build_single(){
	echo "[recall-artefact]----------pre-analyse program $1-------------"
	pre_analyse "$1"
}

#compile the program, extract nativeCallsite
#$1=programName, all tasks are defined in ant_preAnalysis.xml
pre_analyse(){
 check_prerequisite
 cp "${RECALL_ROOT_DIR}"/preAnalysis_script/ant_preAnalysis.xml "${RECALL_ROOT_DIR}"/xcorpus/tools
 #resolve the program dependencies
 eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/exercise.xml resolve > /dev/null 2>&1"
 #run the ant target to extract native callsite
 eval "ant -f ${RECALL_ROOT_DIR}/xcorpus/tools/ant_preAnalysis.xml pre-analysis -Dprogram=$1"
}

check_prerequisite(){
  if [ -z "$JAVA_HOME" ]
    then
      echo "[recall-artefact] Please set \$JAVA_HOME"
      exit 1
  fi
}
#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
