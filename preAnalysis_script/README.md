### preAnalysis.sh

preAnalysis.sh script extracts nativeCallsite from Xcorpus binaries and Java runtime(rt.jar).

Required resources: 

- xcorpus/
- $JAVA_HOME to be set

Required process: none

output: nativeCallsite.txt under the program dir

- running for ALL programs `./preAnalysis_script/preAnalysis.sh` 
- running for ONE program(test program)  `./preAnalysis_script/preAnalysis.sh _test` You can check out list of programs in property.sh
