#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#build all programs.
build_all(){
	echo "[recall-artefact]******start running Doop for all programs******"
	for program in "${programs[@]}" 
	do
		build_single "$program"
	done
}

#build one program
#$1=program
build_single(){
	echo "[recall-artefact]----------running Doop for program $1-------------"
	#check resouces must be presented before running doop
  check_prerequisite "$1"

  #copy the ant build script to the program
  eval "cp ${RECALL_ROOT_DIR}/runDoop_script/ant_packJars.xml ${XCORPUS_DATA_DIR}/$1/.xcorpus"
  ##run doop lib-setup
  # pack all dependencies into app.jar and lib.jar
  eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/ant_packJars.xml make-libJar > /dev/null 2>&1"
  echo "[recall-artefact]---------Doop:runnning lib-setup---------"
  libInput="-i ${RECALL_ROOT_DIR}/data/doop-input/$1/app.jar -l ${RECALL_ROOT_DIR}/data/doop-input/$1/lib.jar ${RECALL_ROOT_DIR}/xcorpus/tools/lib/hamcrest-core-1.3.jar ${RECALL_ROOT_DIR}/xcorpus/tools/lib/junit-4.12.jar"
  resultDir="${RECALL_ROOT_DIR}/data/doop-results/lib-setup"
	run_doop "$1" "${libInput}" "${resultDir}"

	##run doop super-setup
	# pack all dependencies into a super jar
  eval "ant -f ${XCORPUS_DATA_DIR}/$1/.xcorpus/ant_packJars.xml make-superJar > /dev/null 2>&1"
  echo "[recall-artefact]---------Doop:runnning super-setup---------"
  superInput="-i ${RECALL_ROOT_DIR}/data/doop-input/$1/super.jar -l ${RECALL_ROOT_DIR}/xcorpus/tools/lib/hamcrest-core-1.3.jar ${RECALL_ROOT_DIR}/xcorpus/tools/lib/junit-4.12.jar"
  resultDir="${RECALL_ROOT_DIR}/data/doop-results/super-setup"
	run_doop "$1" "${superInput}" "${resultDir}"
	#Soot fails for some ASM classes ("*org.objectweb.asm.*" )and the only solution is to remove them from the input.
	#echo "remove ASM classes from jars" TODO: REMOVE ASM when copy resouces (ant_packJars.xml)
  #zip -q -d ${rootDir}/data/doop-input/$1/super.jar $(jar tf ${rootDir}/data/doop-input/$1/super.jar | grep -F 'org/objectweb/asm/')
  #zip -q -d ${rootDir}/data/doop-input/$1/lib.jar $(jar tf ${rootDir}/data/doop-input/$1/lib.jar | grep -F 'org/objectweb/asm/')
}
#$1=programName
#$2=doopInput
#$3=resultDir
run_doop(){
  PROGRAM=$1
  INPUT=$2
  RESULTS_DIR=$3
  CACHE_DIR="${RECALL_ROOT_DIR}/data/doop-cache"

  #create results folder
  eval "mkdir -p ${RESULTS_DIR}/${PROGRAM}/base"
  eval "mkdir -p ${RESULTS_DIR}/${PROGRAM}/callsite"
  eval "mkdir -p ${RESULTS_DIR}/${PROGRAM}/reflection"
  eval "mkdir -p ${RESULTS_DIR}/${PROGRAM}/reflection-lite"

	echo "[recall-artefact]----Doop:generating facts for ${PROGRAM}"
	start1=`date +%s`
	eval "doop ${INPUT} --platform java_8 --fact-gen-cores 1 --Xstop-at-facts ${CACHE_DIR}/${PROGRAM}-facts --id ${PROGRAM}-facts --Xstats-none --Xsymlink-cached-facts --souffle-jobs 4 --Xignore-wrong-staticness --main EntryPoint_generatedTest EntryPoint_builtinTest ${DOOP_TIME_OUT} > ${CACHE_DIR}/facts-gen-${PROGRAM}.txt"
	end1=`date +%s`
	echo "take $((end1-start1)) secs to generate facts" > "${CACHE_DIR}"/runtime-factGen-"${PROGRAM}".txt

	echo "[recall-artefact]----Doop:context insensitive analysis for ${PROGRAM}"
	start2=`date +%s`
	eval "doop ${INPUT} --platform java_8 -a context-insensitive --Xstats-none --Xsymlink-cached-facts --souffle-jobs 4 --Xstart-after-facts ${CACHE_DIR}/${PROGRAM}-facts --id ${PROGRAM}-base ${DOOP_TIME_OUT} > ${CACHE_DIR}/base-${PROGRAM}.txt"
 	end2=`date +%s`
 	echo "take $((end2-start2)) secs to run context-insensitive" > "${CACHE_DIR}"/runtime-base-"${PROGRAM}".txt

  echo "[recall-artefact]----Doop:call site sensitive for ${PROGRAM}"
  start3=`date +%s`
  eval "doop ${INPUT} --platform java_8 -a 1-call-site-sensitive --Xstats-none --Xsymlink-cached-facts --souffle-jobs 4 --Xstart-after-facts ${CACHE_DIR}/${PROGRAM}-facts --id ${PROGRAM}-callsite ${DOOP_TIME_OUT} > ${CACHE_DIR}/callsite-${PROGRAM}.txt"
  end3=`date +%s`
  echo "take $((end3-start3)) secs to run call-site-sensitive" > "${CACHE_DIR}"/runtime-callsite-"${PROGRAM}".txt

  echo "[recall-artefact]----Doop:full reflection analysis for ${PROGRAM}"
  start4=`date +%s`
  eval "doop ${INPUT} --platform java_8 -a context-insensitive --reflection-classic --reflection-dynamic-proxies --reflection-method-handles --simulate-native-returns --Xstats-none --Xsymlink-cached-facts --souffle-jobs 4 --Xstart-after-facts ${CACHE_DIR}/${PROGRAM}-facts --id ${PROGRAM}-reflection ${DOOP_TIME_OUT} > ${CACHE_DIR}/reflection-${PROGRAM}.txt"
  end4=`date +%s`
  echo "take $((end4-start4)) secs to run full analysis" > "${CACHE_DIR}"/runtime-reflection-"${PROGRAM}".txt

  echo "[recall-artefact]----Doop:lite reflection analysis for ${PROGRAM}"
  start5=`date +%s`
  eval "doop ${INPUT} --platform java_8 -a context-insensitive --light-reflection-glue --distinguish-all-string-constants --reflection-dynamic-proxies --simulate-native-returns --Xstats-none --Xsymlink-cached-facts --souffle-jobs 4 --Xstart-after-facts ${CACHE_DIR}/${PROGRAM}-facts --id ${PROGRAM}-reflection-lite ${DOOP_TIME_OUT} > ${CACHE_DIR}/lite-${PROGRAM}.txt"
  end5=`date +%s`
  echo "take $((end5-start5)) secs to run lite analysis" > "${CACHE_DIR}"/runtime-lite-"${PROGRAM}".txt

  #move all logs to results folder
  eval "mv ${CACHE_DIR}/*.txt ${RESULTS_DIR}/${PROGRAM}"
  #copy results to results folder
  eval "cp ${CACHE_DIR}/context-insensitive/${PROGRAM}-base/database/*.csv ${RESULTS_DIR}/${PROGRAM}/base"
  eval "cp ${CACHE_DIR}/1-call-site-sensitive/${PROGRAM}-callsite/database/*.csv ${RESULTS_DIR}/${PROGRAM}/callsite"
  eval "cp ${CACHE_DIR}/context-insensitive/${PROGRAM}-reflection/database/*.csv ${RESULTS_DIR}/${PROGRAM}/reflection"
  eval "cp ${CACHE_DIR}/context-insensitive/${PROGRAM}-reflection-lite/database/*.csv ${RESULTS_DIR}/${PROGRAM}/reflection-lite"
}

check_prerequisite(){
  if [[ ! -f "${XCORPUS_DATA_DIR}/$1/.xcorpus/drivers-builtin-tests.zip" ]] || [[ ! -f "${XCORPUS_DATA_DIR}/$1/.xcorpus/drivers-generated-tests.zip" ]]
  then
      echo "[recall-artefact] drivers-builtin-tests.zip and drivers-generated-tests.zip must exist! Please run unreflect-tests.sh first "
      exit 1
  fi
}

#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
