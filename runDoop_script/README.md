### doop.sh

doop.sh script runs Doop 4.14.4 to generate Static Call Graphs

Required resources: unreflect tests are produced: xcorpus/data/qualitas_corpus_20130901/${program}/.xcorpus/drivers-\*.zip. All dependencies are packed in data/doop-input

Required process: unreflect-tests

output: Static Call Graphs

- build for ALL programs `./runDoop_script/runDoop.sh` 
- build for ONE program(test program) `./runDoop_script/runDoop.sh _test` You can check out list of programs in property.sh
