### processCCTs.sh

processCCTs.sh script resolves method strings,removes redundant branches. The size of CCTs would be greatly reduced.

Required resources: raw CCTs are presented in data/raw-CCTs-data/

Required process: preAnalysis, unreflect-tests, runTest

output: processed CCTs are produced to data/processed-CCTs-data/

- build for ALL programs `./processCCTs_script/processCCTs.sh` 
- build for ONE program(test program) `./processCCTs_script/processCCTs.sh _test` You can check out list of programs in property.sh
