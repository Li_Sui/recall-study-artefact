#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#build all programs.
build_all(){
	echo "[recall-artefact]******start processing CCTs for all programs******"
	for program in "${programs[@]}" 
	do
		build_single "$program"
	done
}

#build one program
#$1=program
build_single(){
	echo "[recall-artefact]----------processing CCTs for program $1-------------"
	process_CCTs "$1"
}

#process raw CCTs
#$1=programName,
process_CCTs(){
  check_prerequisite "$1"
  #create one if output folder is not exist
  mkdir -p "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"

  echo "[recall-artefact] proccssing raw CCTs,java parameters are set to -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize}"
  #you can change java parameters in property.sh
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.ProcessRawData ${RECALL_ROOT_DIR}/data/raw-CCTs-data/$1-raw-CCTs ${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1"
 
  #after finfished, sleep for 2mins for waiting fies being writing to the disk
  echo "[recall-artefact] sleeping for 2mins.... waiting files to be writen to disk"
  sleep 2m
  
  #after processing all raw CCTs, sort them into builtinTest, generatedTest folder
  mkdir -p "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/CCTs-builtinTest
  mkdir -p "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/CCTs-generatedTest
  mv "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/Driver_builtinTest*.csv "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/CCTs-builtinTest
  mv "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/Driver_generatedTest*.csv "${RECALL_ROOT_DIR}"/data/processed-CCTs-data/"$1"/CCTs-generatedTest


}
#$1=program
check_prerequisite(){
  if [[ ! -d "${RECALL_ROOT_DIR}/data/raw-CCTs-data/$1-raw-CCTs" ]]
  then
      echo "[recall-artefact] ${RECALL_ROOT_DIR}/data/raw-CCTs-data/$1-raw-CCTs must exist!"
      exit 1
  fi
}

#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
