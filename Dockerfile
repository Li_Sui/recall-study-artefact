FROM gfour/doop:4.15

## package update
RUN apt-get update

##install R and installl vioplot package
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get install -y r-base
RUN R -e "install.packages('vioplot')"

##install maven
RUN apt-get install -y maven

##install boost
RUN apt-get install -y libboost-dev

##set working directory
RUN mkdir /recall
WORKDIR /recall

##unzip JDK and ANT
ADD ./libs/* /recall/libs/
RUN unzip -qq /recall/libs/apache-ant-1.9.7.zip
RUN unzip -qq /recall/libs/jdk1.8.0_144.zip

##set environment variables
ENV JAVA_HOME /recall/jdk1.8.0_144
ENV ANT_HOME /recall/apache-ant-1.9.7
ENV PATH ${PATH}:${JAVA_HOME}/bin:${ANT_HOME}/bin

##add build scripts to working directory
ADD ./build.sh /recall
ADD ./property.sh /recall
ADD ./clean.xml /recall
ADD ./xcorpus /recall/xcorpus/
ADD ./java /recall/java
ADD ./unreflectTests_script /recall/unreflectTests_script/
ADD ./preAnalysis_script /recall/preAnalysis_script/
ADD ./processCCTs_script /recall/processCCTs_script/
ADD ./runTest_script /recall/runTest_script/
ADD ./runDoop_script /recall/runDoop_script/
ADD ./findFNs_script /recall/findFNs_script/
ADD ./addNocallsiteTag_script/ /recall/addNocallsiteTag_script/
ADD ./produceStats_script /recall/produceStats_script/

##working dir
ENV RECALL_ROOT_DIR=/recall

##override DOOP_PLATFORMS_LIB, since prepackaged Doop-4.14.4 contains its own JREs
ENV DOOP_PLATFORMS_LIB=$DOOP_HOME
## delete doop-benchmarks, not needed in this configuration
RUN rm -rf $DOOP_BENCHMARKS
##set DOOP_CACHE to /recall/data/doop-cache, where /recall/data is the attached volume
ENV DOOP_CACHE=/recall/data/doop-cache
##set DOOP_OUT to /recall/data/doop-cache, the results will be transferred to /recall/data/doop-results
ENV DOOP_OUT=/recall/data/doop-cache
##move rt.jar,jce.jar,jsse.jar to /DOOP_HOME/JRE
RUN mkdir -p $DOOP_HOME/JREs/jre1.8/lib
RUN cp $JAVA_HOME/jre/lib/rt.jar $DOOP_HOME/JREs/jre1.8/lib
RUN cp $JAVA_HOME/jre/lib/jce.jar $DOOP_HOME/JREs/jre1.8/lib
RUN cp $JAVA_HOME/jre/lib/jsse.jar $DOOP_HOME/JREs/jre1.8/lib

##compile Java code and move them to working directory
RUN mvn -f /recall/java/pom.xml package
RUN cp /recall/java/target/CCT-1.0-driver-generator.jar /recall/xcorpus/tools/lib
RUN cp /recall/java/target/CCT-1.0-instrumentation.jar /recall/xcorpus/tools/lib
RUN cp /recall/java/target/CCT-1.0-pre-analysis.jar /recall/xcorpus/tools/lib
RUN cp /recall/java/target/CCT-1.0-result-analysis.jar /recall/libs
#compile native logger
RUN g++ -std=c++11 -shared -fPIC -I$JAVA_HOME/include -I$JAVA_HOME/include/linux /recall/java/cpp/nativelogger.cpp -o /recall/xcorpus/tools/lib/nativelogger.so

##clean up. can not clean data/ You can run ant -f clean.xml clean-data
RUN rm /recall/libs/apache-ant-1.9.7.zip
RUN rm /recall/libs/jdk1.8.0_144.zip
RUN ant -f clean.xml clean-xcorpus
