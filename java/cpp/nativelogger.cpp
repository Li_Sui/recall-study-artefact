#include <iostream>
#include <fstream>
#include <stack>
#include <vector>
#include <map>
#include <functional>
#include <iterator>
#include <sys/time.h>
#include <atomic>
#include <utility>
#include "nativelogger.h"
#include <boost/functional/hash.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

using namespace std;
class Callgraph{
    stack<string> mystack;
    public:
        void push(string callname){
          mystack.push(callname);
        }
        void pop(){
          mystack.pop();
        }
        string top(){
           return mystack.top();
        }

        int size(){
          return mystack.size();
        }
};

static map <string,Callgraph> threadmap;
static map <int,string> stringMap;
//allocation tag map
static map<int,int> ATM;
static string buffer;
static bool startlogging=false;
static boost::uuids::random_generator gen;

string convertToString(JNIEnv *env,jstring input){
    const char *c_input;
    c_input = env->GetStringUTFChars(input, NULL);
    if(c_input == NULL) {
        return "";
    }
    string converted(c_input);
    env->ReleaseStringUTFChars(input, c_input);
    return converted;
}
void flushBuffer(){
   ofstream myfile;
   myfile.open ("stack.csv",std::ios_base::app);
   myfile << buffer;
   buffer.clear();
   myfile.close();
}

//   output data format:
//   stack.csv:    hashCode,kind,tag,threadID,threadObjectHash\tthreadName\tdepth
//   stringMap.csv hashcode\tclassLoader,className,methodName,descriptor,ID

JNIEXPORT jstring JNICALL
Java_nz_ac_massey_cs_instrumentation_NativeLogger_getInvocationID(JNIEnv *env, jobject obj){
    env->MonitorEnter(obj);
    boost::uuids::uuid id = gen();
    string s=to_string(id);
    char cstr[s.size() + 1];
    strcpy(cstr, s.c_str());
    env->MonitorExit(obj);
    return env->NewStringUTF(cstr);
}

JNIEXPORT void JNICALL
Java_nz_ac_massey_cs_instrumentation_NativeLogger_push(JNIEnv *env, jobject obj,jstring classLoaderName,jstring className,jstring methodName,jstring descriptor,jstring invocationId,
jint kind, jint threadObjectHashCode, jlong threadID,jstring threadName,jint objectHashCode){

    env->MonitorEnter(obj);
    string c_loaderName= convertToString(env,classLoaderName);
    string c_name=convertToString(env,className);
    string m_name=convertToString(env,methodName);
    string desc=convertToString(env,descriptor);
    string inv_id=convertToString(env,invocationId);
    string t_name=convertToString(env,threadName);

    if( (m_name == "main") && (desc == "([Ljava/lang/String;)V") ){
        startlogging=true;
    }
    if(startlogging){
        string SID;
        SID.append(std::to_string((long)threadID));SID.append(",");
        SID.append(std::to_string((int)threadObjectHashCode));
        //if the method is native invocation, assign it with unique id
        if((int)kind==1){
            boost::uuids::uuid id = gen();
            inv_id=to_string(id);
        }
        string base;
        base.append(c_loaderName);base.append(",");
        base.append(c_name);base.append(",");
        base.append(m_name);base.append(",");
        base.append(desc);

        string method;
        boost::hash<std::string> string_hash;
        int base_hash=string_hash(base);
        stringMap.insert(pair<int,string>(base_hash,base));

        method.append(std::to_string(base_hash));method.append(",");method.append(inv_id);method.append(",");
        method.append(std::to_string((int)kind));method.append(",-1");

        //skip native invocation and null reference
        if((int)kind==0 && (int)objectHashCode!=0){
	        //add allocation tag if present
	        map<int,int>::iterator atmIT;
	        for (atmIT=ATM.begin(); atmIT!=ATM.end(); atmIT++){
	            if((int)objectHashCode == atmIT->first){
	            //if there is tag, rebuild the method string
	                method.clear();
	                method.append(std::to_string(base_hash));method.append(",");method.append(inv_id);method.append(",");
	                method.append(std::to_string((int)kind));method.append(",");
	                method.append(std::to_string(atmIT->second));
	                break;
	            }
	        }
    	}
        map<string, Callgraph>::iterator cgIT;
        cgIT=threadmap.find(SID);
        if( cgIT != threadmap.end()){
            cgIT->second.push(std::to_string(base_hash)+","+inv_id);

            buffer.append(method);buffer.append(",");
            buffer.append(SID);buffer.append("\t");
            buffer.append(t_name);buffer.append("\t");
            buffer.append(std::to_string((cgIT->second.size())));
            buffer.append("\n");
            //if buffer exceed 500MB, flush it to a file
            if(buffer.size()> 500000000){
                flushBuffer();
            }

        }else{
            Callgraph cg;
            cg.push(std::to_string(base_hash)+","+inv_id);
            buffer.append(method);buffer.append(",");
            buffer.append(SID);buffer.append("\t");
            buffer.append(t_name);buffer.append("\t1\n");
            threadmap.insert(pair<string,Callgraph>(SID,cg));
            //std::cout <<" entry point: thread["<<tname<<"] ["<<caller<<"]"<<std::endl;
        }
    }
    env->MonitorExit(obj);
}



JNIEXPORT void JNICALL
Java_nz_ac_massey_cs_instrumentation_NativeLogger_pop(JNIEnv *env, jobject obj,jint threadObjectHashCode,jlong threadID){
    env->MonitorEnter(obj);
    if(startlogging){
        string SID;
        SID.append(std::to_string((long)threadID));SID.append(",");
        SID.append(std::to_string((int)threadObjectHashCode));
        map<string, Callgraph>::iterator it;
        it=threadmap.find(SID);
        if(it != threadmap.end()){
            if(it->second.size()>0){
                it->second.pop();
            }
        }
    }
    env->MonitorExit(obj);
}

JNIEXPORT void JNICALL
Java_nz_ac_massey_cs_instrumentation_NativeLogger_addAllocationHash(JNIEnv *env, jobject obj,jint objectHashCode,jint allocationType){
    env->MonitorEnter(obj);
    if(startlogging){
        //skip null reference
        if( (int)objectHashCode!=0){
            map<int, int>::iterator it;
            it=ATM.find((int)objectHashCode);
            if( it != ATM.end()){
                it->second=(int)allocationType;
            }else{
                ATM.insert(pair<int,int>((int)objectHashCode,(int)allocationType));
            }
         }

    }
    env->MonitorExit(obj);
}

JNIEXPORT void JNICALL
Java_nz_ac_massey_cs_instrumentation_NativeLogger_flush(JNIEnv *env, jobject obj){

    env->MonitorEnter(obj);
    ofstream stFile;
    stFile.open ("stack.csv",std::ios_base::app);
    stFile << buffer;
    stFile.close();
    //----------------write string map to file
    ofstream stringMapFile;
    stringMapFile.open("stringMap.csv");
    map<int,string>::iterator it;
    for (it=stringMap.begin(); it!=stringMap.end(); it++){
        stringMapFile << it->first<<"\t"<<it->second<<"\n";
     }
    stringMapFile.close();
    env->MonitorExit(obj);
}

JNIEXPORT void JNICALL
Java_nz_ac_massey_cs_instrumentation_NativeLogger_clear(JNIEnv *env, jobject obj,jstring classLoaderName,jstring className,jstring methodName,jstring descriptor,jstring invocationId,jint threadObjectHashCode, jlong threadID){
 env->MonitorEnter(obj);
    if(startlogging){
        string c_loaderName= convertToString(env,classLoaderName);
        string c_name=convertToString(env,className);
        string m_name=convertToString(env,methodName);
        string desc=convertToString(env,descriptor);
        string inv_id=convertToString(env,invocationId);

        string SID;
        SID.append(std::to_string((long)threadID));SID.append(",");
        SID.append(std::to_string((int)threadObjectHashCode));
        string base;
         base.append(c_loaderName);base.append(",");
         base.append(c_name);base.append(",");
         base.append(m_name);base.append(",");
         base.append(desc);

        boost::hash<std::string> string_hash;
        int base_hash=string_hash(base);
        string currentMethod=std::to_string(base_hash)+","+inv_id;
        map<string, Callgraph>::iterator it;
        it=threadmap.find(SID);
        if(it != threadmap.end()){
            while(it->second.top() != currentMethod){
                it->second.pop();
            }
        }
    }
    env->MonitorExit(obj);
}