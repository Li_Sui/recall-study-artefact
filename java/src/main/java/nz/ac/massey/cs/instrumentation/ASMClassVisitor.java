package nz.ac.massey.cs.instrumentation;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;


public class ASMClassVisitor extends ClassVisitor {
    private String className;
    private String classLoaderName;
    public ASMClassVisitor(String classLoaderName,ClassVisitor cv){
        super(Opcodes.ASM7,cv);
        this.classLoaderName=classLoaderName;
    }


    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        className = name.replace("/",".");
        super.visit(version, access, name, signature, superName, interfaces);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        MethodVisitor mv = cv.visitMethod(access, name, descriptor, signature, exceptions);
        if(isSkipMethod(name)){
            return mv;
        }
        return  mv == null ? null : new ASMMethodVisitor(mv, access, name, descriptor, className,classLoaderName);
    }

    public boolean isSkipMethod(String methodName){
        String m=className+"."+methodName;
        if (m.equals("java.lang.Thread.getName") || m.equals("java.lang.Thread.getId") || m.equals("java.lang.Thread.<init>")
                ||m.equals("java.lang.Thread.init") || m.equals("java.lang.Object.<init>")) {
            return true;
        }
        return false;
    }
}
