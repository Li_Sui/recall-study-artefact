package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

/**
 * recall of base static analysis with respect to orcales obtained by executing built-in,generated and combined tests
 */
public class RQ1Spreadsheet {


    static String OUTPUTHEADER="program,builtin lib,builtin super,generated lib,generated super,combined lib,combined super\n";
    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("inputDir", true, "processed-CCTs-data folder containing all dataset, including programs, CCTs,FNs and invocation list")
                .addOption("outputDir",true,"output folder");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("outputDir")|| !cmd.hasOption("inputDir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + RQ1Spreadsheet.class.getName(), options);
            System.exit(0);
        }

        String inputDir=cmd.getOptionValue("inputDir");
        String outputDir=cmd.getOptionValue("outputDir");
        StringBuilder sb =new StringBuilder(OUTPUTHEADER);

        for(File f: new File(inputDir).listFiles()) {
            if(f.isDirectory()) {
                String program =f.getName();
                sb.append(program + ",");
                getRecall(inputDir + "/" + program, sb);
                sb.append("\n");
            }
        }

        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir+"/RQ1.csv")), Charset.defaultCharset());
    }

    private static void getRecall(String programDir,StringBuilder sb){

        Set<String> combinedFNs_lib=new HashSet<>();
        Set<String> combinedFNs_super=new HashSet<>();
        Set<String> bt_fn_lib=getFNSet(new File(programDir+"/FNs-builtinTest/FNs-contextInsensitive-lib.csv"));
        Set<String> bt_fn_super=getFNSet(new File(programDir+"/FNs-builtinTest/FNs-contextInsensitive-super.csv"));
        Set<String> gt_fn_lib=getFNSet(new File(programDir+"/FNs-generatedTest/FNs-contextInsensitive-lib.csv"));
        Set<String> gt_fn_super=getFNSet(new File(programDir+"/FNs-generatedTest/FNs-contextInsensitive-super.csv"));

        combinedFNs_lib.addAll(bt_fn_lib);combinedFNs_lib.addAll(gt_fn_lib);
        combinedFNs_super.addAll(bt_fn_super);combinedFNs_super.addAll(gt_fn_super);

        Set<String> combinedCG=new HashSet<>();
        Set<String> bt_CG=getCallGraphSet(programDir,"builtinTest");
        Set<String> gt_CG=getCallGraphSet(programDir,"generatedTest");

        combinedCG.addAll(bt_CG);combinedCG.addAll(gt_CG);

        double recall_bt_lib=(bt_CG.size()-bt_fn_lib.size())/Double.valueOf(bt_CG.size());
        double recall_bt_super=(bt_CG.size()-bt_fn_super.size())/Double.valueOf(bt_CG.size());
        double recall_gt_lib=(gt_CG.size()-gt_fn_lib.size())/Double.valueOf(gt_CG.size());
        double recall_gt_super=(gt_CG.size()-gt_fn_super.size())/Double.valueOf(gt_CG.size());
        double recall_combined_lib=(combinedCG.size()-combinedFNs_lib.size())/Double.valueOf(combinedCG.size());
        double recall_combined_super=(combinedCG.size()-combinedFNs_super.size())/Double.valueOf(combinedCG.size());
        sb.append(recall_bt_lib+","+recall_bt_super+","+recall_gt_lib+","+recall_gt_super+","+recall_combined_lib+","+recall_combined_super);


    }

    private static Set<String> getCallGraphSet(String programDir,String testSet){
        String CCTDir=programDir+"/CCTs-"+testSet;
        Set<String> methodSet=new HashSet<>();
        File cctFolder=new File(CCTDir);
        for (File stackData : cctFolder.listFiles()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(stackData))) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] tokens = line.split("\t");
                    assert tokens.length == 3;
                    String[] tokens2 = tokens[0].split(",");
                    assert tokens2.length == 9;

                    String method=tokens2[1]+","+tokens2[2]+","+tokens2[3];
                    methodSet.add(method);
                }
            }catch (Exception e){e.printStackTrace();}
        }
        return methodSet;

    }

    private static Set<String> getFNSet(File file){
        Set<String> fns =new HashSet<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                fns.add(line);
            }
        }catch (Exception e){e.printStackTrace();}

        return fns;
    }

}
