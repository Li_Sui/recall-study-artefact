package nz.ac.massey.cs.resultAnalysis;

import java.util.Objects;

public class Callsite {
    private String name;
    private String descriptor;

    public Callsite(String name,String descriptor){
        this.name=name;
        this.descriptor=descriptor;
    }

    public String getName() {
        return name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Callsite callsite = (Callsite) o;
        return Objects.equals(name, callsite.name) &&
                Objects.equals(descriptor, callsite.descriptor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, descriptor);
    }
}
