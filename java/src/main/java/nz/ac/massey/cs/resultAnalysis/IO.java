package nz.ac.massey.cs.resultAnalysis;

import com.google.common.base.Preconditions;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.lang3.tuple.Pair;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * input data format:
 * classLoader,className,methodName,descriptor,kind,tag,threadID,threadObjectHash,methodID\tthreadName\tdepth
 */
public class IO {


    /**
     * Import data, consisting of a set of FNs, and a stream of call trees built on demand
     */

    public static Pair<Set<Method>,Stream<CallTree>> importDataForProcessing(String[] args) throws Exception {
        Options options = new Options()
                .addOption("fn", true, "file  containing FN (like \"data/ccFns.txt\"")
                .addOption("recordings", true, "root folder containing stack recordings (as csv files)");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("fn") || !cmd.hasOption("recordings")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + IO.class.getName(), options);
            System.exit(0);
        }

        File fnData = new File(cmd.getOptionValue("fn"));
        File recordingsFolder = new File(cmd.getOptionValue("recordings"));
        return importDataForProcessing(fnData,recordingsFolder);
    }
    public static Pair<Set<Method>,Stream<CallTree>> importDataForProcessing(File fnData,File recordingsFolder) throws Exception {

        Preconditions.checkArgument(fnData.exists());
        Preconditions.checkArgument(recordingsFolder.exists());
        Preconditions.checkArgument(recordingsFolder.isDirectory());

        //System.out.println("Importing FNs from " + fnData.getAbsolutePath());
        Set<Method> fns = IO.readFNs(fnData);
        //System.out.println(""+fns.size() + " FNs imported");

        FileFilter filter = f -> f.getName().endsWith(".csv");
        int fileCount = recordingsFolder.listFiles(filter).length;
        AtomicInteger progress = new AtomicInteger();

        // stats:
        Stream stream = Stream.of(recordingsFolder.listFiles(filter)).parallel()
            .map(stackData -> {
                //System.out.println("Importing call tree(s) from " + stackData.getAbsolutePath() + " " + progress.incrementAndGet() + " / " + fileCount);
                Preconditions.checkState(stackData.exists());
                return IO.buildCallTrees(stackData, fns);
            })
            .flatMap(map -> map.values().stream());

        return Pair.of(fns,stream);
    }

    public static Set<Method> readMethods(File file) throws Exception  {
        Set<Method> fns = new HashSet<>();
        Scanner scanner = new Scanner(file);
        scanner.useDelimiter("\n");
        while (scanner.hasNext()) {
            String next = scanner.next();
            String[] tokens = next.split("\t");
            Method method = Method.getInstance("",tokens[0].split(",")[0],tokens[0].split(",")[1],tokens[0].split(",")[2]);
            fns.add(method);
        }
        scanner.close();
        return fns;
    }


    public static Set<Method> readFNs(File file) throws Exception  {
        return readMethods(file);
    }

    /**
     * Build the call trees, return them as map. The keys are the  threads.
     * @param file the data  file
     * @param fns the false negatives
     * @return
     * @throws Exception
     */
    public static Map<String,CallTree> buildCallTrees (File file,Set<Method> fns)  {
        String l=null;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;

            Map<String, Stack<Invocation>> stacks = new HashMap<>();
            Map<String, CallTree> callTrees = new HashMap<>();

            int count = 0;
            while ((line = reader.readLine()) != null) {
                l=line;
                String[] tokens = line.split("\t");
                assert tokens.length == 3;
                String[] tokens2 = tokens[0].split(",");
                assert tokens2.length == 9;
                count++;

//                if (count % 200_000 == 0) {
//                    System.out.println("\trecords processed: " + count);
//                }

                Method method = Method.getInstance(tokens2[0], tokens2[1], tokens2[2],tokens2[3]);

                boolean isNative = tokens2[4].equals("native");
                String tags = tokens2[5];
                String threadId=tokens2[6];
                String threadObjectHash=tokens2[7];
                String id = tokens2[8];
                String threadName = tokens[1];
                CallTree callTree = callTrees.computeIfAbsent(threadId, t -> new CallTree(threadId, file.getName()));
                Stack<Invocation> stack = stacks.computeIfAbsent(threadId, t -> new Stack<Invocation>());

                int depth = Integer.parseInt(tokens[2]);

                Invocation invocation = new Invocation(method, id, threadName,threadId,threadObjectHash, tags, fns.contains(method), count, depth, isNative);
                Invocation rootInv=new Invocation(Method.ROOT_METHOD,"-1",threadName,threadId,threadObjectHash,"null",false,-1,0,false);

                //set root
                if (!callTree.hasRoot()) {
                    assert invocation.getDepth() == 1;
                    callTree.setRoot(rootInv);
                }
                //if depth is 1, added to root
                if(depth==1){
                    callTree.getRoot().addChild(invocation);
                }

                // optional logging code

//            if (invocation.isFN()) {
//                System.out.println("\tFN invocation encountered: " + invocation);
//            }

//            if (tags!=null) {
//                System.out.println("\tadded record imported from line #" + count + " , tags is: " + tags);
//            }

                while (depth <= stack.size()) {
                        stack.pop();
                }
                Invocation callsite = stack.isEmpty()?null:stack.peek();
                stack.push(invocation);
                assert stack.size()==invocation.getDepth();
                if (callsite!=null) {
                    assert callsite.getDepth()+1 == invocation.getDepth();
                    callTree.addEdge(callsite, invocation);
                }

            }
//            System.out.println("\tdone, " + count + " lines processed");
            reader.close();
            return callTrees;
        }
        catch (Exception x) {
            x.printStackTrace();
            return null;
        }
    }

    private static void stackInvariantViolated(File file,int lineNo,String details) {
        //System.err.println("\tStack invariant violated processing file " + file.getName() + "line #" + lineNo + " -- details: " + details);
    }

    /**
     *  This method is used to process large data set.
     *  output stack data for further analysis.
     * @param outputDir
     * @param args
     * @throws Exception
     */
    public static void removeBranchesFromLargeDataSet(String outputDir, String[] args) throws Exception{
        Options options = new Options()
                .addOption("recordings", true, "root folder containing stack recordings (as csv files)");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("recordings")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + IO.class.getName(), options);
            System.exit(0);
        }

        File recordingsFolder = new File(cmd.getOptionValue("recordings"));
        Preconditions.checkArgument(recordingsFolder.exists());
        Preconditions.checkArgument(recordingsFolder.isDirectory());

        FileFilter filter = f -> f.getName().endsWith(".csv");
        int fileCount = recordingsFolder.listFiles(filter).length;
        AtomicInteger progress = new AtomicInteger();
        for (File stackData : recordingsFolder.listFiles(filter)) {
            System.out.println("Importing call tree(s) from " + stackData.getAbsolutePath() + " " + progress.incrementAndGet() + " / " + fileCount);
            Preconditions.checkState(stackData.exists());
            Map<String,CallTree> callTreeMap=IO.buildCallTrees(stackData, new HashSet<>());

            for(String threadID: callTreeMap.keySet()){
                CallTree callTree=callTreeMap.get(threadID);
                CallTreeReducer.removeRedundantBranches(callTree);
                CallTreeReducer.removeInstrumentationBranches(callTree,Method.INSTRUMENTATION);
                IO.recreateStackFileAfterReduction(outputDir,callTree);
            }
        }
    }

    public static void recreateStackFileAfterReduction(String outputDir, CallTree callTree) throws IOException{
        Predicate<Invocation> filter = inv -> true;
        FileWriter fw = new FileWriter(outputDir+'/'+callTree.getDataSource(),true);
        BufferedWriter bw = new BufferedWriter(fw);
        callTree.dfs(filter, inv -> {
            if(inv.getDepth()!=0) {
                Method method = inv.getMethod();

                String kind = inv.getIsNative() ? "native" : "none-native";
                try {
                    // bw.write(method.getClassLoader()+","+method.getClassName() + "," + method.getName() + "," + method.getDescriptor() + "," + kind + "," + inv.getTag() + "," + inv.getThreadId()+","+inv.getThreadObjectHash()+","+inv.getInvocationId() + "\t" + inv.getThreadName() + "\t" + inv.getDepth()+"\n");
                    bw.write(method.getClassLoader());
                    bw.write(",");
                    bw.write(method.getClassName());
                    bw.write(",");
                    bw.write(method.getName());
                    bw.write(",");
                    bw.write(method.getDescriptor());
                    bw.write(",");
                    bw.write(kind);
                    bw.write(",");
                    bw.write(inv.getTag());
                    bw.write(",");
                    bw.write(inv.getThreadId());
                    bw.write(",");
                    bw.write(inv.getThreadObjectHash());
                    bw.write(",");
                    bw.write(inv.getInvocationId());
                    bw.write("\t");
                    bw.write(inv.getThreadName());
                    bw.write("\t");
                    bw.write(String.valueOf(inv.getDepth()));
                    bw.write("\n");
                } catch (IOException e) {
                }
            }
        });
        bw.close();
        fw.close();

    }

}
