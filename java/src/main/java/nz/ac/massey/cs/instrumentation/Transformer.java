package nz.ac.massey.cs.instrumentation;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.instrument.ClassFileTransformer;

import java.security.ProtectionDomain;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

public class Transformer implements ClassFileTransformer {


    @Override
    public byte[] transform(ClassLoader classLoader, String className, Class<?> aClass, ProtectionDomain protectionDomain, byte[] bytes) {
        if (className.startsWith("nz/ac/massey/cs/instrumentation") || className.startsWith("org/objectweb/asm")|| className.equals("module-info")){
            return bytes;
        }

        //if(className.startsWith("Driver") || className.startsWith("sfti/gaps/fn") || className.startsWith("nz/ac/massey/cs/example")) {
        return instrument(bytes, classLoader,className);
        //}
       //return bytes;
    }



    private byte[] instrument(byte[] b,ClassLoader classLoader,String name) {
        ClassWriter writer=null;
        try {
            String classLoaderName = (classLoader == null) ? "systemLoader" : classLoader.toString();
            ClassReader reader = new ClassReader(b);
            ClassVersionVisitor versionVisitor= new ClassVersionVisitor();
            reader.accept(versionVisitor,0);
            int computeFrame;
            if(versionVisitor.isRightVersion()){
                computeFrame=ClassWriter.COMPUTE_FRAMES;
            }else{
                computeFrame=ClassWriter.COMPUTE_MAXS;
            }

            writer = new ComputeClassWriter(computeFrame,classLoader);

            reader.accept(new ASMClassVisitor(classLoaderName, writer), ClassReader.SKIP_FRAMES);

        }catch (Throwable e){
            //if something go wrong return the clean bytecode
            return b;
        }
        return writer.toByteArray();
    }
}
