package nz.ac.massey.cs.preanalysis;

import javassist.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * pre-analyse classes to extract all native methods.
 * will be used for instrumentation to match callsite.
 *
 * args[0]=input dir(jars or zips) args[1]=path to rt.jar args[2]=output file
 * @author li sui
 */
public class NativeMethodFinder {

    public static void main(String[] args) throws Exception{
        String inputDir =args[0];
        String runtimeJar=args[1];
        String outputFile =args[2];
        // iterate a dir to find all zips and jars
        Iterator it =FileUtils.iterateFiles(new File(inputDir),null,true);
        Set<String> callsite=new HashSet<>();
        while(it.hasNext()){
            File f = (File) it.next();
            parseZip(f,callsite);
        }
        // extract call sites from java runtime(rt.jar)
        parseZip(new File(runtimeJar),callsite);
        StringBuilder sb =new StringBuilder();
        for(String c: callsite){
            sb.append(c+"\n");
        }
        FileUtils.writeStringToFile(new File(outputFile),sb.toString(), Charset.defaultCharset());
    }

    private static void parseZip(File f,Set<String> callsite) throws Exception{
        if(f.getName().endsWith(".jar") || f.getName().endsWith(".zip")){
            ZipFile zipFile = new ZipFile(f.getPath());

            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            int count=0;
            while(entries.hasMoreElements()){
                ZipEntry entry = entries.nextElement();
                if(entry.getName().endsWith(".class")) {
                    InputStream stream = zipFile.getInputStream(entry);
                    count=count+readClass(IOUtils.toByteArray(stream), callsite);
                }
            }
            System.out.println("extracting:"+f.getPath()+"-"+count);
        }
    }

    private static int readClass(byte[] b,Set<String> callsite){
        ClassPool pool = ClassPool.getDefault();
        CtClass clazz = null;
        int count=0;
        try {
            clazz = pool.makeClass(new ByteArrayInputStream(b));
            CtBehavior[] methods = clazz.getDeclaredBehaviors();

            for (int i = 0; i < methods.length; i++) {
                if(Modifier.isNative(methods[i].getModifiers())){
                    String className=clazz.getName();
                    String methodName = methods[i].getName();
                    String name = className.substring(className.lastIndexOf('.') + 1, className.length());
                    String signature=methods[i].getSignature();
                    if (methods[i].getName().equals(name)) {
                        methodName = "<init>";
                    }
                    String callname=className+"+"+methodName+"+"+signature;
                    callsite.add(callname);
                    count++;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (clazz != null) {
                clazz.detach();
            }
        }

        return count;
    }
}
