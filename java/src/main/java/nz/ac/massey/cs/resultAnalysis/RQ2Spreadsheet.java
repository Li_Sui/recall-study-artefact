package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

public class RQ2Spreadsheet {


    static String OUTPUTHEADER="program,builtin base,builtin contextsensitive,generated base,generated contextsensitive,combined base,combined contextsensitive\n";
    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("inputDir", true, "processed-CCTs-data folder containing all dataset, including programs, CCTs,FNs and invocation list")
                .addOption("outputDir",true,"output folder");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("outputDir")|| !cmd.hasOption("inputDir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + RQ2Spreadsheet.class.getName(), options);
            System.exit(0);
        }

        String inputDir=cmd.getOptionValue("inputDir");
        String outputDir=cmd.getOptionValue("outputDir");
        StringBuilder sb =new StringBuilder(OUTPUTHEADER);
        for(File f: new File(inputDir).listFiles()) {
            if(f.isDirectory()) {
                String program =f.getName();
                sb.append(program + ",");
                getRecall(inputDir + "/" + program, sb);
                sb.append("\n");
            }
        }

        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir+"/RQ2.csv")), Charset.defaultCharset());
    }

    private static void getRecall(String programDir,StringBuilder sb){

        Set<String> combinedFNs_base=new HashSet<>();
        Set<String> combinedFNs_callsite=new HashSet<>();
        Set<String> bt_fn_base=getFNSet(new File(programDir+"/FNs-builtinTest/FNs-contextInsensitive-lib.csv"));
        Set<String> bt_fn_callsite=getFNSet(new File(programDir+"/FNs-builtinTest/FNs-contextSensitive-lib.csv"));
        Set<String> gt_fn_base=getFNSet(new File(programDir+"/FNs-generatedTest/FNs-contextInsensitive-lib.csv"));
        Set<String> gt_fn_callsite=getFNSet(new File(programDir+"/FNs-generatedTest/FNs-contextSensitive-lib.csv"));

        combinedFNs_base.addAll(bt_fn_base);combinedFNs_base.addAll(gt_fn_base);
        combinedFNs_callsite.addAll(bt_fn_callsite);combinedFNs_callsite.addAll(gt_fn_callsite);

        Set<String> combinedCG=new HashSet<>();
        Set<String> bt_CG=getCallGraphSet(programDir,"builtinTest");
        Set<String> gt_CG=getCallGraphSet(programDir,"generatedTest");

        combinedCG.addAll(bt_CG);combinedCG.addAll(gt_CG);

        double recall_bt_base=(bt_CG.size()-bt_fn_base.size())/Double.valueOf(bt_CG.size());
        double recall_bt_callsite=(bt_CG.size()-bt_fn_callsite.size())/Double.valueOf(bt_CG.size());
        double recall_gt_base=(gt_CG.size()-gt_fn_base.size())/Double.valueOf(gt_CG.size());
        double recall_gt_callsite=(gt_CG.size()-gt_fn_callsite.size())/Double.valueOf(gt_CG.size());
        double recall_combined_base=(combinedCG.size()-combinedFNs_base.size())/Double.valueOf(combinedCG.size());
        double recall_combined_callsite=(combinedCG.size()-combinedFNs_callsite.size())/Double.valueOf(combinedCG.size());
        sb.append(recall_bt_base+","+recall_bt_callsite+","+recall_gt_base+","+recall_gt_callsite+","+recall_combined_base+","+recall_combined_callsite);


    }

    private static Set<String> getCallGraphSet(String programDir,String testSet){
        String CCTDir=programDir+"/CCTs-"+testSet;
        Set<String> methodSet=new HashSet<>();
        File cctFolder=new File(CCTDir);
        for (File stackData : cctFolder.listFiles()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(stackData))) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] tokens = line.split("\t");
                    assert tokens.length == 3;
                    String[] tokens2 = tokens[0].split(",");
                    assert tokens2.length == 9;

                    String method=tokens2[1]+","+tokens2[2]+","+tokens2[3];
                    methodSet.add(method);
                }
            }catch (Exception e){e.printStackTrace();}
        }
        return methodSet;

    }

    private static Set<String> getFNSet(File file){
        Set<String> fns =new HashSet<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                fns.add(line);
            }
        }catch (Exception e){e.printStackTrace();}

        return fns;
    }
}
