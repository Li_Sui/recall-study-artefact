package nz.ac.massey.cs.resultAnalysis;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility to remove branches from call trees.
 * @author jens dietrich
 */
public class CallTreeReducer {

    /**
     * Remove redundant branches from calltrees, those branches are caused by invoking methods in loops.
     * @param callTree
     */
    public static void removeRedundantBranches(CallTree callTree) {

        callTree.getRoot().computeStructuralHash();
        // System.out.println("Structural hashes computed");
        Map<Integer,Invocation> structuralHashes = Collections.synchronizedMap(new HashMap<>());
        Set<Invocation> rootsOfRedundantBranches = Collections.synchronizedSet(new HashSet<>());

        callTree.dfsParallel(inv -> true,inv -> {
            structuralHashes.clear();
            rootsOfRedundantBranches.clear();
            for (Invocation nextValue:inv.getChildren()) {
                Invocation previousValue = structuralHashes.put(nextValue.getStructuralHash(),nextValue);
                if (previousValue!=null) {
                    if (nextValue.structuralSameAs(previousValue)) {
                        rootsOfRedundantBranches.add(previousValue);
                    }
                }
            }
            if (rootsOfRedundantBranches.size()>0) {
                // delete with one traversal
                Iterator<Invocation> iterator = inv.getChildren().iterator();
                while (iterator.hasNext()) {
                    Invocation next = iterator.next();
                    if (rootsOfRedundantBranches.contains(next)) {
                        iterator.remove();
                    }
                }
            }
        });

    }



    /**
     * Remove branches with roots in the set of selected methods.
     * @param callTree
     * @param methods
     */
    public static void removeBranches(CallTree callTree,Set<Method> methods) {
        // clusy code to avoid concurrent modification exception
        Set<Invocation> toBeRemoved = Collections.newSetFromMap(new ConcurrentHashMap<>());
        callTree.dfsParallel(inv -> {
                if (methods.contains(inv.getMethod())) {
                    toBeRemoved.add(inv);
                    return false;
                }
                return true;
            },inv -> {}
        );
        for (Invocation inv:toBeRemoved) {
            Invocation parent = inv.getParent();
            if (parent!=null) {
                boolean success = parent.removeChild(inv);
                assert success;
            }
        }
    }

    /**
     * Remove branches with roots in the set of selected methods.
     * @param callTree
     * @param methods
     */
    public static void removeBranches(CallTree callTree,Method... methods) {
        Set<Method> mm = Stream.of(methods).collect(Collectors.toSet());
        removeBranches(callTree,mm);
    }

    /**
     * Remove branches with roots in the set of selected methods.
     * @param callTree
     * @param methods
     */
    public static void removeInstrumentationBranches(CallTree callTree,Method... methods) {
        Set<Method> mm = Stream.of(methods).collect(Collectors.toSet());
        removeBranches(callTree,mm);
    }


}
