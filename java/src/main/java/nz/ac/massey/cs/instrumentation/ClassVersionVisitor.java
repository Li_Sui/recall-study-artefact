package nz.ac.massey.cs.instrumentation;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;

public class ClassVersionVisitor extends ClassVisitor implements Opcodes{
    boolean isRightVersion=true;
    public ClassVersionVisitor() {
        super(Opcodes.ASM7);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        if(version ==V1_1 || version==V1_2 || version== V1_3|| version==V1_4 || version== V1_5){
            isRightVersion=false;
        }
        super.visit(version, access, name, signature, superName, interfaces);
    }

    public boolean isRightVersion(){
        return  isRightVersion;
    }
}
