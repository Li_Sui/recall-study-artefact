package nz.ac.massey.cs.instrumentation;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AdviceAdapter;


import java.util.ArrayList;
import java.util.List;

public class ASMMethodVisitor extends AdviceAdapter {

    private static String nativeLogger="nz/ac/massey/cs/instrumentation/NativeLogger";

    private String classLoaderName;
    private String className;
    private String methodName;
    private String descriptor;
    private List<Label> startOfCatchBlocks;
    private int variableID;
    private boolean isStaticMethod;
    public ASMMethodVisitor(MethodVisitor mv, int access, String methodName, String desc, String className,String classLoaderName){
        super(Opcodes.ASM7,mv,access,methodName,desc);
        this.classLoaderName=classLoaderName;
        this.className=className;
        this.methodName=methodName;
        this.descriptor=desc;
        this.startOfCatchBlocks=new ArrayList<>();
        this.isStaticMethod=(access & (Opcodes.ACC_STATIC))==Opcodes.ACC_STATIC;
    }


    @Override
    protected void onMethodEnter() {
        //create id for each method
        this.variableID=this.newLocal(Type.getType(String.class));
         Label l0 = new Label();
         mv.visitLabel(l0);
         mv.visitMethodInsn(INVOKESTATIC, nativeLogger, "getInvocationID", "()Ljava/lang/String;", false);
         mv.visitVarInsn(ASTORE, this.variableID);
         //push
         injectPUSH(false,this.className,this.methodName,this.descriptor);//none native method
    }

    @Override
    protected void onMethodExit(int opcode) {
        //don't inject throws, clear() function will handle pop operation
        if(ATHROW!=opcode) {

            if(className.equals("java.lang.Class") && methodName.equals("newInstance")){//allocation
                injectAddAllocationHash(ICONST_0);
            }
            if(className.equals("java.lang.reflect.Constructor") && methodName.equals("newInstance")){//allocation
                injectAddAllocationHash(ICONST_1);
            }
            if(className.equals("java.io.ObjectInputStream") && methodName.equals("readObject")){//allocation
                injectAddAllocationHash(ICONST_2);
            }
            if(className.equals("java.lang.reflect.Field") && methodName.equals("get")){//dynamic access
                injectAddAllocationHash(ICONST_3);
            }
           injectPOP();
        }
    }

    @Override
    public void visitLabel(Label label) {
        super.visitLabel(label);
        if(startOfCatchBlocks.contains(label)){
           injectCLEAR();
        }
    }


    //visit native invocation
    @Override
    public void visitMethodInsn(int opcodeAndSource, String owner, String name, String descriptor, boolean isInterface) {
        //ignore native callsites within a constructor
        if(!methodName.equals("<init>")) {
            String ownerClass = owner.replace("/", ".");
            StringBuilder sb = new StringBuilder();
            sb.append(ownerClass);
            sb.append("+");
            sb.append(name);
            sb.append("+");
            sb.append(descriptor);

            if (Agent.nativeCallsite.contains(sb.toString()) || (ownerClass.equals("java.lang.invoke.MethodHandle") && name.startsWith("invoke"))) { //hard coded to include this native call.reason:java.lang.invoke.MethodHandle$PolymorphicSignature
                injectPUSH(true, ownerClass, name, descriptor);//is a native call
                super.visitMethodInsn(opcodeAndSource, owner, name, descriptor, isInterface);
                //if the callsite return an object, get that object's hashcode
                if (descriptor.split("\\)")[1].startsWith("L") && descriptor.endsWith(";")) {
                    //if it is sun.misc.unsafe.getObject, we tag it as allocation.
                    if (ownerClass.equals("sun.misc.Unsafe") && name.equals("getObject")) {//allocation
                        injectAddAllocationHash(ICONST_4);
                    } else {
                        injectAddAllocationHash(ICONST_5);
                    }
                }
                injectPOP();
            } else {
                super.visitMethodInsn(opcodeAndSource, owner, name, descriptor, isInterface);
            }
        }else{
            super.visitMethodInsn(opcodeAndSource, owner, name, descriptor, isInterface);
        }
    }

    //visit try-catch block
    @Override
    public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
        startOfCatchBlocks.add(handler); //the beginning of catch block;
        super.visitTryCatchBlock(start, end, handler, type);
    }


    public void injectAddAllocationHash(int type){
        Label l0 = new Label();
        mv.visitLabel(l0);
        mv.visitInsn(Opcodes.DUP);//returned object;
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "identityHashCode", "(Ljava/lang/Object;)I", false);
        mv.visitInsn(type);//allocation type(0,1,2,3,4)
        mv.visitMethodInsn(INVOKESTATIC, nativeLogger, "addAllocationHash", "(II)V", false);
    }

    public void injectPOP(){
        Label l0 = new Label();
        mv.visitLabel(l0);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "identityHashCode", "(Ljava/lang/Object;)I", false);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Thread", "getId", "()J", false);
        mv.visitMethodInsn(INVOKESTATIC, nativeLogger, "pop", "(IJ)V", false);
    }


    public void injectPUSH(boolean isNative,String className,String methodName,String descriptor){
        Label l0 = new Label();
        mv.visitLabel(l0);
        mv.visitLdcInsn(this.classLoaderName);
        mv.visitLdcInsn(className);
        mv.visitLdcInsn(methodName);
        mv.visitLdcInsn(descriptor);
        mv.visitVarInsn(ALOAD, this.variableID);//invocation id
        if(isNative) {
            mv.visitInsn(ICONST_1);//kind
        }else{
            mv.visitInsn(ICONST_0);
        }
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "identityHashCode", "(Ljava/lang/Object;)I", false);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Thread", "getId", "()J", false);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Thread", "getName", "()Ljava/lang/String;", false);
        if(isStaticMethod || isNative){//skip comparing native invocation and static methods
            mv.visitInsn(ICONST_0);
        }else {
            mv.visitVarInsn(ALOAD, 0);//this object
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "identityHashCode", "(Ljava/lang/Object;)I", false);
        }
        mv.visitMethodInsn(INVOKESTATIC, nativeLogger, "push", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIJLjava/lang/String;I)V", false);
    }
    public void injectCLEAR(){
        Label l0 = new Label();
        mv.visitLabel(l0);
        mv.visitLdcInsn(this.classLoaderName);
        mv.visitLdcInsn(this.className);
        mv.visitLdcInsn(this.methodName);
        mv.visitLdcInsn(this.descriptor);
        mv.visitVarInsn(ALOAD, this.variableID);//invocation id
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "identityHashCode", "(Ljava/lang/Object;)I", false);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "currentThread", "()Ljava/lang/Thread;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Thread", "getId", "()J", false);
        mv.visitMethodInsn(INVOKESTATIC, nativeLogger, "clear", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V", false);
    }

}
