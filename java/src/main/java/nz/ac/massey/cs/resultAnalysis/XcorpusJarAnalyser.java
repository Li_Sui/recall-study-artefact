package nz.ac.massey.cs.resultAnalysis;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtBehavior;
import javassist.CtClass;
import javassist.expr.ConstructorCall;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import javassist.expr.NewExpr;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *  analysis Jars and extract all methods and callsites from xcorpus. xcorpus need to be pre-compiled. This is used for detecting nosuchcallsite in CCTs
 *
 *
 *  output data format:
 *  className,methodName,descriptor\tcallsiteName,callsiteDescriptor#......
 *
 */
public class XcorpusJarAnalyser {

    private static ClassPool pool = ClassPool.getDefault();
    public static void main(String[] args)throws Exception{

        if(args.length==0){
            System.err.println("args[0]=program dir, args[1]=java runtime location, args[2]=output dir");
            return;
        }
        String programDir=args[0];
        String rt=args[1];
        String outputDir=args[2];


        Set<String> program_invocatonSet=new HashSet<>();
        program_invocatonSet=readClasses(programDir+"/.xcorpus/build/main");
        program_invocatonSet.addAll(readClasses(programDir+"/.xcorpus/build/builtinTestDriver"));
        program_invocatonSet.addAll(readClasses(programDir+"/.xcorpus/build/generatedTestDriver"));
        Set<String> lib_invocationSet=new HashSet<>();
        lib_invocationSet= readJar(programDir+"/.xcorpus/lib");
        File default_lib=new File(programDir+"/project/default-lib");
        if(default_lib.exists()){
            lib_invocationSet.addAll(readJar(programDir+"/project/default-lib"));
        }
        Set<String> jdk_invocationSet=readJavaRT(rt);
        writeToFile(program_invocatonSet,outputDir,"program-invocationList.csv");
        writeToFile(lib_invocationSet,outputDir,"library-invocationList.csv");
        writeToFile(jdk_invocationSet,outputDir,"jdk1.8.0_144-invocationList.csv");


    }
    private static Set<String> readClasses(String dir) throws Exception{
        Iterator it = FileUtils.iterateFiles(new File(dir),null,true);
        Set<String> methods=new HashSet<>();
        while(it.hasNext()) {
            File f = (File) it.next();
            if (f.getName().endsWith(".class")) {
                analysisClass(IOUtils.toByteArray(new FileInputStream(f)),methods);
            }
        }
        return methods;
    }
    private static Set<String> readJavaRT(String path) throws Exception{
        File rt=new File(path);
        Set<String> methods=new HashSet<>();
        if(rt.getName().equals("rt.jar")){
            System.out.println("analysing:" + rt.getPath());
            ZipFile zipFile = new ZipFile(rt.getPath());
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.getName().endsWith(".class")) {
                    InputStream stream = zipFile.getInputStream(entry);
                    analysisClass(IOUtils.toByteArray(stream),methods);
                }
            }
        }
        return methods;
    }

    private static Set<String> readJar(String dir) throws Exception{
        Iterator it = FileUtils.iterateFiles(new File(dir),null,true);
        Set<String> methods=new HashSet<>();
        while(it.hasNext()) {
            File f = (File) it.next();
            if (f.getName().endsWith(".jar") || f.getName().endsWith(".zip")) {
                System.out.println("analysing:" + f.getPath());
                ZipFile zipFile = new ZipFile(f.getPath());
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().endsWith(".class")) {
                        InputStream stream = zipFile.getInputStream(entry);
                        analysisClass(IOUtils.toByteArray(stream),methods);
                    }
                }
            }
        }
        return methods;
    }

    private static void analysisClass(byte[] b, Set<String> methods){
        CtClass clazz = null;
        try {
            clazz = pool.makeClass(new ByteArrayInputStream(b));
            if(!clazz.isAnnotation() && !clazz.isArray()) {
                CtBehavior[] m = clazz.getDeclaredBehaviors();

                for (int i = 0; i < m.length; i++) {

                    String className = clazz.getName();
                    String methodName = m[i].getName();
                    String name = className.substring(className.lastIndexOf('.') + 1, className.length());
                    String signature = m[i].getSignature();
                    if (m[i].getName().equals(name)) {
                        methodName = "<init>";
                    }

                    StringBuilder sb =new StringBuilder();
                    sb.append(className);sb.append(",");
                    sb.append(methodName);sb.append(",");
                    sb.append(signature);sb.append("\t");
                    //remove duplicate callsite
                    Set<String> callsites=new HashSet<>();
                    m[i].instrument(new ExprEditor() {
                        public void edit(MethodCall m) throws CannotCompileException {
                            callsites.add(m.getMethodName()+","+m.getSignature());
                        }

                        @Override
                        public void edit(NewExpr e) throws CannotCompileException {
                            callsites.add("<init>,"+e.getSignature());
                        }

                        @Override
                        public void edit(ConstructorCall c) throws CannotCompileException {
                            //exclude super call to java.lang.object.<init>
                            if(!c.getClassName().equals("java.lang.Object")) {
                                callsites.add("<init>," + c.getSignature());
                            }
                        }
                    });


                    for(String callsite:callsites){
                        sb.append(callsite);
                        sb.append("#");
                    }
                    methods.add(sb.toString());
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (clazz != null) {
                clazz.detach();
            }
        }
    }

    private static void writeToFile(Set<String> methods,String outputDir,String fileName) throws Exception{
        StringBuilder sb =new StringBuilder();
        for(String m: methods){
            sb.append(m+"\n");
        }
        FileUtils.writeStringToFile(new File(outputDir+"/"+fileName),sb.toString(), Charset.defaultCharset());
    }
}
