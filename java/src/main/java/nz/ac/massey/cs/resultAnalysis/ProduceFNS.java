package nz.ac.massey.cs.resultAnalysis;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;


import java.io.*;
import java.nio.charset.Charset;

import java.util.*;

/**
 *  produce FNs on given doop reachables. Generate list of FNs that can not be referenced
 *
 *  it will generate FNs for builtin-tests and generated-tests set.
 *
 */
public class ProduceFNS {


    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("recordings", true, "root folder containing processed CCTs")
                .addOption("doopLib",true,"root folder containing Doop-lib results")
                .addOption("doopSuper",true,"root folder containing Doop-super results");


        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("doopLib")|| !cmd.hasOption("recordings") ||!cmd.hasOption("doopSuper")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + ProduceFNS.class.getName(), options);
            System.exit(0);
        }

        String recordings=cmd.getOptionValue("recordings");
        //produce FNs
        produceFNsWithLibSetup(recordings,cmd.getOptionValue("doopLib"));
        //produce FNs
        produceFNsWithSuperSetup(recordings,cmd.getOptionValue("doopSuper"));



    }

    private static void produceFNsWithLibSetup(String programDir, String doopPath) throws Exception{
        String CCTDir=programDir;
        File cctFolder=new File(CCTDir);
        Doop doop =new Doop("lib",doopPath);
        FNs(cctFolder,"Driver_builtinTest",doop,programDir+"/FNs-builtinTest");

        FNs(cctFolder,"Driver_generatedTest",doop,programDir+"/FNs-generatedTest");

    }

    private static void produceFNsWithSuperSetup(String programDir, String doopPath) throws Exception{
        String CCTDir=programDir;
        File cctFolder=new File(CCTDir);
        Doop doop =new Doop("super",doopPath);

        FNs(cctFolder,"Driver_builtinTest",doop,programDir+"/FNs-builtinTest");

        FNs(cctFolder,"Driver_generatedTest",doop,programDir+"/FNs-generatedTest");

    }

    private static void FNs(File cctFolder, String filter, Doop doop,String outputDir) throws Exception{
        Set<String> base_FNS=new HashSet<>();
        Set<String> callsite_FNS=new HashSet<>();
        Set<String> reflection_FNS=new HashSet<>();
        Set<String> reflectionLite_FNS=new HashSet<>();

        Set<String> doopBase_set=doop.getDoopBase_set();
        Set<String> doopCallsite_set=doop.getDoopCallsite_set();
        Set<String> doopReflection_set=doop.getDoopReflection_set();
        Set<String> doopReflectionLite_set=doop.getDoopReflectionLite_set();
        Iterator<File> iterator=FileUtils.iterateFiles(cctFolder,null,true);
        while(iterator.hasNext()){
            File stackData =iterator.next();
            if(stackData.getName().contains(filter)) {
                try (BufferedReader reader = new BufferedReader(new FileReader(stackData))) {
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        String[] tokens = line.split("\t");
                        assert tokens.length == 3;
                        String[] tokens2 = tokens[0].split(",");
                        assert tokens2.length == 9;

                        String method = tokens2[1] + "," + tokens2[2] + "," + tokens2[3];
                        if (!doopBase_set.isEmpty() && !doopBase_set.contains(method)) {
                            base_FNS.add(method);
                        }
                        if (!doopCallsite_set.isEmpty() && !doopCallsite_set.contains(method)) {
                            callsite_FNS.add(method);
                        }
                        if (!doopReflection_set.isEmpty() && !doopReflection_set.contains(method)) {
                            reflection_FNS.add(method);
                        }
                        if (!doopReflectionLite_set.isEmpty() && !doopReflectionLite_set.contains(method)) {
                            reflectionLite_FNS.add(method);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
        writeToFile(base_FNS,"contextInsensitive-"+doop.getSetup(),outputDir);
        writeToFile(callsite_FNS,"contextSensitive-"+doop.getSetup(),outputDir);
        writeToFile(reflection_FNS,"reflection-"+doop.getSetup(),outputDir);
        writeToFile(reflectionLite_FNS,"reflectionLite-"+doop.getSetup(),outputDir);
    }

    private static void writeToFile(Set<String> FNS,String type,String outputDir) throws Exception{

        StringBuilder sb = new StringBuilder();
        for (String method : FNS) {
            sb.append(method + "\n");
        }
        File directory =new File(outputDir);
        if(!directory .exists()){
            directory.mkdir();
        }
        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir+"/FNs-"+type+".csv")),Charset.defaultCharset());
    }



}
