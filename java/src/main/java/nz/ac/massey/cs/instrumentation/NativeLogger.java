package nz.ac.massey.cs.instrumentation;

public class NativeLogger {
    public static synchronized native void push(String classLoaderName, String className, String methodName, String descriptor,String invocationID,int kind,int threadObjectHashCode,long threadID, String threadName,int objectHashCode);
    public static synchronized native void pop(int threadObjectHashCode,long threadID);
    public static synchronized native void clear(String classLoaderName, String className, String methodName, String descriptor,String invocationID,int threadObjectHashCode,long threadID);
    public static synchronized native void addAllocationHash(int objectHashCode,int allocationType);
    public static synchronized native void flush();
    public static synchronized native String getInvocationID();
}
