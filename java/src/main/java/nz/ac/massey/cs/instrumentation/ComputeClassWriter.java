package nz.ac.massey.cs.instrumentation;

import org.objectweb.asm.ClassWriter;

public class ComputeClassWriter extends ClassWriter {
    private ClassLoader classLoader;

    public ComputeClassWriter(final int flags,final ClassLoader classLoader) {
        super(flags);
        this.classLoader=classLoader;
    }

    @Override
    protected String getCommonSuperClass(final String className1, final String className2) {
        return "java/lang/Object";
    }
}
