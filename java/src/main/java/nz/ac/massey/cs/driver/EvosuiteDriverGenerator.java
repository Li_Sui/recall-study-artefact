package nz.ac.massey.cs.driver;


import javassist.*;
import javassist.bytecode.*;

import javassist.expr.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;



import java.io.ByteArrayInputStream;
import java.io.File;

import java.io.FileInputStream;
import java.nio.charset.Charset;


import java.util.*;

/**
 * This class is used to remove all junit+evosuite references from generated-tests in Xcorpus
 *
 * args[0]=inputDir args[1]=outputDir
 *
 */
public class EvosuiteDriverGenerator {
    static Set<CtMethod> evosuite;
    public static void main(String[] args) throws Exception{
        if(args.length==0){
            System.err.println("args[0]=inputDir args[1]=outputDir");
            return;
        }
        String inputDir=args[0];
        String outputDir=args[1];

        Iterator it = FileUtils.iterateFiles(new File(inputDir),null,true);
        int countRemove=0;
        int countTotal=0;
        int clazzCounter=0;
        List<String> driverClazz=new ArrayList<>();
        ClassPool pool = ClassPool.getDefault();
        while(it.hasNext()){
            File f = (File) it.next();

            if(f.getName().endsWith("_ESTest.class")) {

                CtClass clazz = null;
                try {
                    clazz = pool.makeClass(new ByteArrayInputStream(IOUtils.toByteArray(new FileInputStream(f))));


                    final String className=clazz.getName().split("\\_ESTest")[0];
                    evosuite=new HashSet<>();
                    //remove class annotation
                    AnnotationsAttribute clazzAttr=(AnnotationsAttribute)clazz.getClassFile().getAttribute(AnnotationsAttribute.visibleTag);
                    clazzAttr.removeAnnotation("org.junit.runner.RunWith");
                    clazzAttr.removeAnnotation("org.evosuite.runtime.EvoRunnerParameters");

                   // reset super class
                    clazz.setSuperclass(pool.get("java.lang.Object"));

                    CtConstructor constructor=clazz.getConstructor("()V");
                    constructor.instrument(new ExprEditor(){
                        @Override
                        public void edit(ConstructorCall c) throws CannotCompileException {
                            //remove super() reference to evosuite
                            if(c.getClassName().endsWith("_ESTest_scaffolding")) {
                                c.replace("");
                            }
                        }
                    });

                    CtMethod[] methods = clazz.getDeclaredMethods();
                    countTotal=countTotal+methods.length;
                    for (int i = 0; i < methods.length; i++) {
                        final CtMethod method =methods[i];
                        MethodInfo minfo = method.getMethodInfo();
                        AnnotationsAttribute methodAttr = (AnnotationsAttribute)
                                minfo.getAttribute(AnnotationsAttribute.visibleTag);
                        //remove method annotation
                        methodAttr.removeAnnotation("org.junit.Test");

                        method.instrument(new ExprEditor() {
                            @Override
                            public void edit(MethodCall m) throws CannotCompileException {
                                //remove method call to evosuite
                                if (m.getClassName().startsWith("org.evosuite.runtime")) {
                                    evosuite.add(method);
                                }
                                //remove reference to junit
                                if (m.getClassName().equals("xcorpus.AssertionAdapter")) {
                                    m.replace("");
                                }
                            }
                            @Override
                            public void edit(Handler h) throws CannotCompileException {
                                //remove reference to evosuite in catch clause
                                try {
                                    if(h.getType().getName().startsWith("org.evosuite.runtime")){
                                        evosuite.add(method);
                                    }
                                }catch (NotFoundException e){
                                    //in case evosuite is not in classpath
                                    if(e.getMessage().startsWith("org.evosuite.runtime")){
                                        evosuite.add(method);
                                    }
                                }
                            }
                            @Override
                            public void edit(NewExpr e) throws CannotCompileException {
                                //remove new instance reference to evosuite
                                if(e.getClassName().startsWith("org.evosuite.runtime")){
                                    evosuite.add(method);
                                }
                            }
                            @Override
                            public void edit(Cast c) throws CannotCompileException {
                                //remove cast reference to evosuite
                                try {
                                    if(c.getType().getName().startsWith("org.evosuite.runtime")){
                                        evosuite.add(method);
                                    }
                                    //in case evosuite is not in classpath
                                }catch (NotFoundException e){
                                    if(e.getMessage().startsWith("org.evosuite.runtime")){
                                        evosuite.add(method);
                                    }
                                }
                            }
                        });
                    }

                    for(CtMethod ctm: evosuite){
                        clazz.removeMethod(ctm);
                    }

                    countRemove=countRemove+evosuite.size();

                    //check if a class contain no method

                    if(clazz.getDeclaredMethods().length!=0){
                        clazz.setName(className+"_mod");
                        clazz.writeFile(outputDir);

                        //construct driver
                        String driverClassName="Driver_generatedTest"+clazzCounter;
                        StringBuilder driverSourcecode=new StringBuilder();

                        driverSourcecode.append("public class "+driverClassName+"{\n");


                        driverSourcecode.append("\tpublic static void main(String[] args) throws Throwable{\n");
                        for(int i=0;i<clazz.getDeclaredMethods().length;i++){
                            driverSourcecode.append("\t\ttry{ new "+clazz.getName()+"()."+clazz.getDeclaredMethods()[i].getName()+"();\n\t\t}catch(Throwable e){System.err.println(e.getMessage());}\n");
                        }
                        driverSourcecode.append("\t}\n");

                        driverSourcecode.append("}");
                        driverClazz.add(driverClassName);

                        FileUtils.writeStringToFile(new File(outputDir+"/"+driverClassName+".java"),driverSourcecode.toString(),Charset.defaultCharset());
                        clazzCounter++;

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    if (clazz != null) {
                        clazz.detach();

                    }
                }

            }
        }

        //create entry point for static analysis
        StringBuilder entryPointSource=new StringBuilder();
        entryPointSource.append("public class EntryPoint_generatedTest{\n");
        entryPointSource.append("public static void main(String[] args) throws Throwable{\n");
        for (String driver : driverClazz) {
            entryPointSource.append(driver+".main(null);\n");
        }
        entryPointSource.append("}\n");
        entryPointSource.append("}");
        System.out.println("generated "+clazzCounter+" evosuite drivers");
        FileUtils.writeStringToFile(new File(outputDir+"/EntryPoint_generatedTest.java"),entryPointSource.toString(), Charset.defaultCharset());



        FileUtils.writeStringToFile(new File(outputDir+"/driverLog.txt"),"removed:"+countRemove+"  total:"+countTotal,Charset.defaultCharset());
    }
}
