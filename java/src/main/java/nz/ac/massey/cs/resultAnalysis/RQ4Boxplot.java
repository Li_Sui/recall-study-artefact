package nz.ac.massey.cs.resultAnalysis;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * generate spreadsheet for RQ4. the cause of recall. In this spreadsheet, we have 4 categories: invocation, allocation, dynamic access and implicit call
 */
public class RQ4Boxplot {
    //match exact java.lang.reflect.Method.invoke
    public static boolean isReflectionInvoke(Method method){
        return method.equals(Method.METHOD_INVOKE);
    }
    //match all java.lang.invoke.MethodHandle.invoke*
    public static boolean isMethodHandleInvoke(Method method){
        return method.getClassName().equals("java.lang.invoke.MethodHandle") && method.getName().startsWith("invoke");
    }
    //match invokedynamic
    public static boolean isInvokedynamic(Method method){
        return method.getName().contains("lambda$");
    }
    //match all InvocationHandler.invoke
    public static boolean isPorxyInvoke(Method method){
        return method.getDescriptor().equals("(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;") && method.getName().equals("invoke");
    }

    //match invocation
    public static boolean isInvocation(Method method){
        return isReflectionInvoke(method) || isMethodHandleInvoke(method) || isPorxyInvoke(method) || isInvokedynamic(method);
    }
    //match dynamic allocations
    public static boolean isAllocation(String tag){
        return isClassNewInstance(tag) || isConstructorNewInstance(tag) || isSerialisation(tag) || isUnsafe(tag) || isNativeReturn(tag);
    }
    public static boolean isClassNewInstance(String tag){
        return tag.equals("Class.newInstance");
    }
    public static boolean isConstructorNewInstance(String tag){
        return tag.equals("Constructor.newInstance");
    }
    public static boolean isSerialisation(String tag){
        return tag.equals("ObjectInputStream.readObject");
    }
    public static boolean isUnsafe(String tag){
        return tag.equals("Unsafe.getObject");
    }
    public static boolean isNativeReturn(String tag){
        return tag.equals("native.return");
    }

    //match Field.get
    public static boolean isDynamicAccess(String tag){
        return tag.equals("Field.get");
    }

    public static boolean isImplicit(String tag,String threadName){
        return matchNoSuchCallSite(tag) || matchSystemThread(threadName);
    }
    //match no such call site in a give list
    public static boolean matchNoSuchCallSite(String tag){
        return tag.equals("nocallsite");
    }
    //match already known system threads
    public static boolean matchSystemThread(String threadName){
        List<String> systemThread=Arrays.asList("AWT-EventQueue-0","AWT-Shutdown","Finalizer","DestroyJavaVM","Reference Handler","Signal Dispatcher");
        return systemThread.contains(threadName);
    }


    public static boolean matchAllCategories(Method method,String tag,String threadName){
        return isImplicit(tag,threadName) || isDynamicAccess(tag) || isAllocation(tag) || isInvocation(method);
    }
    static String OUTPUTHEADER="program, dynamic invocation,dynamic allocation,dynamic access, implicit, uncategorized \n";
    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("inputDir", true, "processed-CCTs-data folder containing all dataset, including programs, CCTs,FNs and invocation list")
                .addOption("outputDir",true,"output folder");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("outputDir")|| !cmd.hasOption("inputDir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + RQ4Boxplot.class.getName(), options);
            System.exit(0);
        }

        String inputDir=cmd.getOptionValue("inputDir");
        String outputDir=cmd.getOptionValue("outputDir");
        StringBuilder sb =new StringBuilder(OUTPUTHEADER);
        for(File f: new File(inputDir).listFiles()) {
            if(f.isDirectory()) {
                String program = f.getName();
                sb.append(program + ",");
                String analysis = "contextInsensitive";
                Set<String> combinedFNs_ref = new HashSet<>();
                Set<String> bt_fn_ref = getFNSet(new File(inputDir + "/" + program + "/FNs-builtinTest/FNs-" + analysis + "-lib.csv"));
                Set<String> gt_fn_ref = getFNSet(new File(inputDir+ "/" + program + "/FNs-generatedTest/FNs-" + analysis + "-lib.csv"));
                combinedFNs_ref.addAll(bt_fn_ref);
                combinedFNs_ref.addAll(gt_fn_ref);
                int combinednoFns = combinedFNs_ref.size();
                processFNs(combinednoFns, inputDir+ "/" + program, sb, analysis);
                sb.append("\n");
            }
        }

        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir+"/RQ4-boxplot-base.csv")), Charset.defaultCharset());
        sb=new StringBuilder(OUTPUTHEADER);
        for(File f: new File(inputDir).listFiles()) {
            if(f.isDirectory()) {
                String program = f.getName();
                sb.append(program + ",");
                String analysis = "reflection";
                Set<String> combinedFNs_ref = new HashSet<>();
                Set<String> bt_fn_ref = getFNSet(new File(inputDir + "/" + program + "/FNs-builtinTest/FNs-" + analysis + "-lib.csv"));
                Set<String> gt_fn_ref = getFNSet(new File(inputDir + "/" + program + "/FNs-generatedTest/FNs-" + analysis + "-lib.csv"));
                combinedFNs_ref.addAll(bt_fn_ref);
                combinedFNs_ref.addAll(gt_fn_ref);
                int combinednoFns = combinedFNs_ref.size();
                processFNs(combinednoFns, inputDir + "/" + program, sb, analysis);
                sb.append("\n");
            }
        }
        IOUtils.write(sb.toString(),new FileOutputStream(new File(outputDir+"/RQ4-boxplot-reflection.csv")), Charset.defaultCharset());
    }

    public static synchronized void processFNs(int combinednoFns,String programDir,StringBuilder sb,String analysis) throws Exception{
        Set<Method> combined_invocation=new HashSet<>();
        Set<Method> combined_allocation=new HashSet<>();
        Set<Method> combined_dynAccess=new HashSet<>();
        Set<Method> combined_implicit=new HashSet<>();
        Set<Method> bt_invocation=getInvocationFNs(programDir+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-builtinTest");
        Set<Method> gt_invocation=getInvocationFNs(programDir+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-generatedTest");

        Set<Method> bt_allocation=getAllocationFNs(programDir+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-builtinTest");
        Set<Method> gt_allocation=getAllocationFNs(programDir+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-generatedTest");


        Set<Method> bt_dynAccess=getDynAccessFNs(programDir+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-builtinTest");
        Set<Method> gt_dynAccess=getDynAccessFNs(programDir+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-generatedTest");

        Set<Method> bt_implicit=getImplicitFNs(programDir+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-builtinTest");
        Set<Method> gt_implicit=getImplicitFNs(programDir+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-generatedTest");
        combined_invocation.addAll(bt_invocation);combined_invocation.addAll(gt_invocation);
        combined_allocation.addAll(bt_allocation);combined_allocation.addAll(gt_allocation);
        combined_dynAccess.addAll(bt_dynAccess);combined_dynAccess.addAll(gt_dynAccess);
        combined_implicit.addAll(bt_implicit);combined_implicit.addAll(gt_implicit);

        double invocation_FNs= Double.valueOf(combined_invocation.size())/Double.valueOf(combinednoFns);

        double allocation_FNs= Double.valueOf(combined_allocation.size())/Double.valueOf(combinednoFns);
        double dynAccess_FNs= Double.valueOf(combined_dynAccess.size())/Double.valueOf(combinednoFns);
        double implicit_FNs= Double.valueOf(combined_implicit.size())/Double.valueOf(combinednoFns);

        Set<Method> combined_All=new HashSet<>();
        Set<Method> bt_all=getAllCategoriesFNs(programDir+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-builtinTest");
        Set<Method> gt_all=getAllCategoriesFNs(programDir+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv",programDir+"/CCTs-generatedTest");
        combined_All.addAll(bt_all);combined_All.addAll(gt_all);

        double uncategorized_FNs=Double.valueOf(combinednoFns-combined_All.size())/Double.valueOf(combinednoFns);

        sb.append(invocation_FNs+","+allocation_FNs+","+dynAccess_FNs+","+implicit_FNs+","+uncategorized_FNs);
    }

    public static Set<Method> getAllocationFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};

        //remove branches that caused by dynamic allocation
        Predicate<Invocation> allocation = inv -> !isAllocation(inv.getTag());

        return getRemovedFNs(allocation,arg);
    }

    public static Set<Method> getInvocationFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};

        //remove branches that caused by reflective invocation
        Predicate<Invocation> invocation = inv ->!isInvocation(inv.getMethod());

        return getRemovedFNs(invocation,arg);
    }
    public static Set<Method> getDynAccessFNs(String fnFile,String recordings) throws Exception{

        //remove branches that caused by dynamic access
        Predicate<Invocation> dynamicAccess = inv -> !isDynamicAccess(inv.getTag());
        String[] arg= {"-fn",fnFile,"-recordings",recordings};

        return getRemovedFNs(dynamicAccess,arg);
    }
    public static Set<Method> getImplicitFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        //remove branches that caused by invocation without such callsite
        Predicate<Invocation> nosuchcallsite = inv -> !isImplicit(inv.getTag(),inv.getThreadName());

        return getRemovedFNs(nosuchcallsite,arg);
    }

    public static Set<Method> getAllCategoriesFNs(String fnFile,String recordings) throws Exception{
        String[] arg= {"-fn",fnFile,"-recordings",recordings};
        //removed branched that caused by all categories
        Predicate<Invocation> allCategories = inv -> !matchAllCategories(inv.getMethod(),inv.getTag(),inv.getThreadName());

        return getRemovedFNs(allCategories,arg);
    }

    public static Set<Method> getRemovedFNs(Predicate<Invocation> filter, String[] arg) throws Exception{
        Pair<Set<Method>, Stream<CallTree>> data = IO.importDataForProcessing(arg);
        Set<Method> fnsDetectedAfterFiltering = Collections.synchronizedSet(new HashSet<>());
        Set<Method> fns=data.getLeft();
        data.getRight()
                .forEach(
                        callTree -> {
                            callTree.dfsParallel(filter, inv -> {
                                if (inv.isFN()) {
                                    fnsDetectedAfterFiltering.add(inv.getMethod());
                                }
                            });
                        }
                );
        fns.removeAll(fnsDetectedAfterFiltering);
        return fns;
    }

    private static Set<String> getFNSet(File file){
        Set<String> fns =new HashSet<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                fns.add(line);
            }
        }catch (Exception e){e.printStackTrace();}

        return fns;
    }
}
