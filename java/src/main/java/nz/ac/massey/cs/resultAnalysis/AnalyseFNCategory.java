package nz.ac.massey.cs.resultAnalysis;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * categorise FNs
 * method.invoke,lambda,handler.invoke,dynproxy.invoke,class.newinstance,constr.newinstance,deserialize,unsafe.getobject,nativeallocation,field.get,nocallsite,systemthread,other
 * @author Li Sui
 */
public class AnalyseFNCategory {

    static List<String> analysiss= Arrays.asList("contextInsensitive","reflection");

    public static void main(String[] args) throws Exception{
        Options options = new Options()
                .addOption("inputDir", true, "processed-CCTs-data folder containing all dataset, including programs, CCTs,FNs and invocation list")
                .addOption("outputDir",true,"output folder");
        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("outputDir")|| !cmd.hasOption("inputDir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> <options> " + RQ3Spreadsheet.class.getName(), options);
            System.exit(0);
        }

        String inputDir=cmd.getOptionValue("inputDir");
        String outputDir=cmd.getOptionValue("outputDir");


        for(String analysis: analysiss) {
            StringBuilder category =new StringBuilder("program,DI,DALL,DACC,SYS,other\n");
            StringBuilder detailedCategory=new StringBuilder("program,method.invoke,lambda,handler.invoke,dynproxy.invoke,class.newinstance,constr.newinstance,deserialize,unsafe.getobject,nativeallocation,field.get,nocallsite,systemthread,other\n");
            for(File f: new File(inputDir).listFiles()) {
                if(f.isDirectory()) {
                String program =f.getName();
                Output output = new AnalyseFNCategory().categoriseFNs(getFNs(inputDir, program, analysis).size(), inputDir + "/" + program, analysis);
                category.append(program+","+output.getCategory());
                category.append("\n");
                detailedCategory.append(program+","+output.getDetailedCategory());
                detailedCategory.append("\n");
                }
            }
            IOUtils.write(category.toString(),new FileOutputStream(new File(outputDir+"/RQ4-boxplot-"+analysis+".csv")), Charset.defaultCharset());
            IOUtils.write(detailedCategory.toString(),new FileOutputStream(new File(outputDir+"/RQ4-category-detailed-"+analysis+".csv")), Charset.defaultCharset());

        }
    }

    private static Set<String> getFNs(String rootDir,String program,String analysis){
        Set<String> combinedFNs=new HashSet<>();
        Set<String> btFnSet=readFNFile(new File(rootDir+"/"+program+"/FNs-builtinTest/FNs-"+analysis+"-lib.csv"));
        Set<String> gtFnSet=readFNFile(new File(rootDir+"/"+program+"/FNs-generatedTest/FNs-"+analysis+"-lib.csv"));
        combinedFNs.addAll(btFnSet);combinedFNs.addAll(gtFnSet);
        return combinedFNs;
    }

    private Output categoriseFNs(int totoalNoFns,String programDir,String analysis){

        Set<String> dynInvocation=new HashSet<>();
        //dynamic invocation
        Set<String> methodInvoke=new HashSet<>();
        Set<String> lambda=new HashSet<>();
        Set<String> handlerInvoke =new HashSet<>();
        Set<String> dynproxyInvoke=new HashSet<>();

        Set<String> dynAllocation=new HashSet<>();
        //dynamic allocation
        Set<String> classNewinstance=new HashSet<>();
        Set<String> constrNewinstance=new HashSet<>();
        Set<String> deserialize=new HashSet<>();
        Set<String> unsafeGetobject=new HashSet<>();
        Set<String> nativeAllocation=new HashSet<>();

        //dynamic access
        Set<String> fieldGet=new HashSet<>();

        Set<String> sys=new HashSet<>();
        //SYS
        Set<String> nocallsite=new HashSet<>();
        Set<String> systemThread=new HashSet<>();
        //other(not categorised)
        Set<String> other=new HashSet<>();

        File categoryFNFile =new File(programDir+"/FNS-tagged-lib-combined-"+analysis+".csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(categoryFNFile ))) {
            String line = null;
            while ((line = reader.readLine()) != null) {

                String[] element=line.split(",");
                //classname,methodname,descriptor
                String fn=element[0]+","+element[1]+","+element[2];
                String tag=element[3];
                if(tag.equals("method.invoke")){
                    methodInvoke.add(fn);
                }
                if(tag.equals("lambda")){
                    lambda.add(fn);
                }
                if(tag.equals("handler.invoke")){
                    handlerInvoke.add(fn);
                }
                if(tag.equals("dynproxy.invoke")){
                    dynproxyInvoke.add(fn);
                }
                if(tag.equals("class.newinstance")){
                    classNewinstance.add(fn);
                }
                if(tag.equals("constr.newinstance")){
                    constrNewinstance.add(fn);
                }
                if(tag.equals("deserialize")){
                    deserialize.add(fn);
                }
                if(tag.equals("unsafe.getobject")){
                    unsafeGetobject.add(fn);
                }
                if(tag.equals("nativeallocation")){
                    nativeAllocation.add(fn);
                }
                if(tag.equals("field.get")){
                    fieldGet.add(fn);
                }
                if(tag.equals("nocallsite")){
                    nocallsite.add(fn);
                }
                if(tag.equals("systemthread")){
                    systemThread.add(fn);
                }
                if(tag.equals("other")){
                    other.add(fn);
                }
            }
        }catch (Exception e){e.printStackTrace();}

        dynInvocation.addAll(methodInvoke);dynInvocation.addAll(lambda);dynInvocation.addAll(handlerInvoke);dynInvocation.addAll(dynproxyInvoke);
        dynAllocation.addAll(classNewinstance);dynAllocation.addAll(constrNewinstance);dynAllocation.addAll(deserialize);dynAllocation.addAll(nativeAllocation);dynAllocation.addAll(unsafeGetobject);
        sys.addAll(systemThread);sys.addAll(nocallsite);

        StringBuilder category =new StringBuilder();
        StringBuilder detailedCategory=new StringBuilder();

        detailedCategory.append(Double.valueOf(methodInvoke.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(lambda.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(handlerInvoke.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(dynproxyInvoke.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(classNewinstance.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(constrNewinstance.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(deserialize.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(unsafeGetobject.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(nativeAllocation.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(fieldGet.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(nocallsite.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(systemThread.size())/Double.valueOf(totoalNoFns)+",");
        detailedCategory.append(Double.valueOf(other.size())/Double.valueOf(totoalNoFns));

        category.append(Double.valueOf(dynInvocation.size())/Double.valueOf(totoalNoFns)+",");
        category.append(Double.valueOf(dynAllocation.size())/Double.valueOf(totoalNoFns)+",");
        category.append(Double.valueOf(fieldGet.size())/Double.valueOf(totoalNoFns)+",");
        category.append(Double.valueOf(sys.size())/Double.valueOf(totoalNoFns)+",");
        category.append(Double.valueOf(other.size())/Double.valueOf(totoalNoFns));

        Output output=new Output(category,detailedCategory);

        return output;
    }
    private static Set<String> readFNFile(File file){
        Set<String> fns =new HashSet<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                fns.add(line);
            }
        }catch (Exception e){e.printStackTrace();}

        return fns;
    }


    public class Output{
        StringBuilder category=new StringBuilder();
        StringBuilder detailedCategory =new StringBuilder();
        public Output(StringBuilder category,StringBuilder detailedCategory) {
            this.category=category;
            this.detailedCategory=detailedCategory;
        }
        public StringBuilder getDetailedCategory() {
            return detailedCategory;
        }
        public StringBuilder getCategory() {
            return category;
        }
    }
}
