# Java code for recall-study-artefact

* instrumentation code 
* unreflected tests code
* result process code

Build:

```
mvn clean package
```

## Instrumentation

- ### Handling Thread and Exception


    * Thread: each thread has a unique method stack. We use System.identityHashCode(Thread.currentThread()) to differentiate thread objects
    * Exception: insert a statement to clear the stack up to the current method. e.g.
        ```    
        catch(Exception e){
        clear();// inserted during the instrumenation
        method();
        }
        ```


- ### Class identifier

    In multi-classloader environments the plain name of a class does not unambiguously identify a class. [Ref](https://www.jacoco.org/jacoco/trunk/doc/implementation.html)
    
    The information of current class loader is concatenate with method name when push onto the stack.
    ```
    sun.misc.Launcher$AppClassLoader@18b4aac2,com.example.Class,method,()V
    ```

- ### Native Logger

    To re-compile C++ in Linux. (Note that there is a hard copy of complied Native Logger in xcorpus/tools/lib). Required lib: libboost
    
    ```
    g++ -std=c++11 -shared -fPIC -I$JAVA_HOME/include -I$JAVA_HOME/include/linux cpp/nativelogger.cpp -o nativelogger.so
    ```
    
    To collect all native callsites
    ```
    java -cp target/CCT-1.0-pre-analysis.jar nz.ac.massey.cs.preanalysis.NativeMethodFinder [Dir of jars] [path of rt.jar] [output file]
    ```
    
    To generate call graph. Note that the Instrumentaion only works on jre1.8.**
    
    ```
    java -Xbootclasspath/p:target/CCT-1.0-instrumentation.jar -javaagent:target/CCT-1.0-instrumentation.jar=nativeCallsite.txt test.Example
    ```


- ### Limitation

    There are some noises produced by the agent.  Instrumentation.retransformClasses() re-transforms classes that already loaded by JVM. This retransformation triggers transform() method to inject code and installed as the new definition of the class.
     - re-transformation transforms classes one by one.
     - the instrumentation has calls into runtime API (e.g. java.util.LinkedList) which has been transformed and installed.
     - Javaassit has calls into runtime API which has been transformed and installed.
    
    To deal with it, we apply filter to remove disconnect graphs(assume program entry is `main(String[] args))`

## Generate Driver (unreflected tests)

Build built-in tests

```
java -cp target/CCT-1.0-driver-generator.jar nz.ac.massey.cs.driver.BuiltinTestDriverGenerator[inputDir] [outputDir]
```

Build generated tests

```
java -cp target/CCT-1.0-driver-generator.jar nz.ac.massey.cs.driver.EvosuiteDriverGenerator [inputDir] [outputDir]
```

## Results processing

includes: 

* processing raw CCTs
* add `nosuchcallsite` tag to CCTs
* generate stats
* generate FNs

## Processed CCT data format

the data format is defined as follow:

```
classLoaderName,className,methodName,(parameterType)returnType,kind,tag,threadID,threadObjectHash,methodID \t threadName \t depth
```

note: kind=(none-nativeInvocation | nativeInvocation), tag=(Class.newInstance| Constructor.newInstance| ObjectInputStream.readObject |Field.get |Unsafe.getObject| native.return| noTag),methodID= a unique identified for this invocation, depth=1 is the root

e.g. `systemLoader,java.lang.ThreadGroup,add,(Ljava/lang/Thread;)V,none-native,noTag,11,1304359947,1f6cb0d5-ddbf-468c-8c0a-012eb2979228 \t DestroyJavaVM \t 1`