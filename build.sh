#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

#build all programs, including: generate drivers, run/record CCTs, reduce and filter CCTs, extract reachable methods from CCTs, build SCG with doop, extract reachable methods from SCG, diff reachable method to find FNs
build_all(){
	for program in "${programs[@]}" 
	do
		build_single "$program"
	done
}

#build one program
#$1=program
build_single(){
  #dynamic analysis part
	bash "${RECALL_ROOT_DIR}"/preAnalysis_script/preAnalysis.sh "$1"
  bash "${RECALL_ROOT_DIR}"/unreflectTests_script/unreflect-tests.sh "$1"
  bash "${RECALL_ROOT_DIR}"/runTest_script/runTests.sh "$1"
  bash "${RECALL_ROOT_DIR}"/processCCTs_script/processCCTs.sh "$1"
  #static analysis part
  bash "${RECALL_ROOT_DIR}"/runDoop_script/runDoop.sh "$1"
  #diff reachable
  bash "${RECALL_ROOT_DIR}"/findFNs_script/findFNs.sh "$1"
  #add tag to CCTs
  bash "${RECALL_ROOT_DIR}"/addNocallsiteTag_script/addNocallsiteTag.sh "$1"
  #clean doop-cache
  eval "ant -f ${RECALL_ROOT_DIR}/clean.xml clean-doop-cache"
}

#check to see if input is in pre-defined program list
#$1 =program
checkIfInProgramList(){
  isProgram=false
  for input in "${programs[@]}"
  do
    if [ "${input}" == "$1" ] || [ "$1" == "_test" ] ## if input is _test, run test program
      then
        isProgram=true;
    fi
  done
  if [ ${isProgram} == false ]
  then
    echo "[recall-artefact] $1 is not a program, list of programs is defined in properties.sh "
    exit 1
  fi
}

#main
main(){
  import_property

  if [ $# == 0 ]
  then
    build_all
  fi

  if [ $# == 1 ]
  then
    checkIfInProgramList "$1"
    build_single "$1"
  fi
  if [ $# -gt 1 ]
  then
    for p in "$@"
    do
      checkIfInProgramList "$p"
      build_single "$p"
    done
  fi
}

main $@
