package xcorpus;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Character;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathConstants;
import org.xml.sax.InputSource;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.core.ZipFile;

/*


*/

/*
The ReportAggregator analyzes for each xcorpus project the newest existing report and aggregates them into a central one for the whole corpus.
Report format is:
<report name="foo 3.2.1">
	...packages...

	<counter type="INSTRUCTION" missed="25118" covered="255000/>
	<counter type="BRANCH" missed="2726" covered="25000"/>
	<counter type="LINE" missed="6847" covered="23432"/>
	<counter type="COMPLEXITY" missed="2982" covered="20432"/>
	<counter type="METHOD" missed="787" covered="1232"/>
	<counter type="CLASS" missed="118" covered="1000"/>
</report>

*/
public class ReportAggregator{

	public static void main(String[] args){
		execute(args);
	}

	private static List<File> getFoldersWithName(List<File> projects, String folderName){
		List<File> ret = new LinkedList<File>();

		File temp = new File("");
		for(File project:projects){
			if(project.getName().equals("_test")|!project.isDirectory()){
				continue;
			}

			temp = new File(project.toString() + "/.xcorpus");
			if(temp.exists()){		
				temp = new File(temp.toString() + "/" + folderName);
				if(temp.exists()){
					ret.add(temp);
				}
			}
			else {
				List<File> subOutputFolders = getFoldersWithName(Arrays.asList(project.listFiles()), folderName);
				ret.addAll(subOutputFolders);
			}
		}

		return ret;
	}

	public static boolean isNumeric(String str){
	    for (char c : str.toCharArray())
	    {
		if (!Character.isDigit(c)) 
			return false;
	    }
	    return true;
	}

	private static List<File> ReportSearch(List<File> outputFolders){
		List<File> ret = new LinkedList<File>();	
		File temp = new File("");

		if(outputFolders.isEmpty()){
			return ret;
		}

		for(File folder:outputFolders){
			File[]tempoutput = folder.listFiles();	//outputContent are the different folders in the output folder. This can be timestamps or subreports.
			List<File> outputContent=new ArrayList<File>();
			if(tempoutput.length > 0){
				for(int i=0;i<tempoutput.length;i++){// remove none numeric folder
					if(isNumeric(tempoutput[i].getName())){
						outputContent.add(tempoutput[i]);
					}
				}
				
					
					Collections.sort(outputContent);
					
					// only consider newest timestamp to make sure only the right reports are returned
					if(outputContent.get(outputContent.size()-1).exists()){
						// case 1: no subprojects
						temp = new File(outputContent.get(outputContent.size()-1).toString() + "/report.xml");
						
						if(temp.exists()){
							ret.add(temp);
						}

						else{
							File[] subProjects = outputContent.get(outputContent.size()-1).listFiles();

							for(File f : subProjects){
								if (f.exists()){
									temp = new File(f.toString() + "/report.xml");
								}
		
								if(temp.exists()){
									ret.add(temp);
								}
							}
						}
					}
				
			
			}
		}
		return ret;
	}

	private static List<File> recursiveEvosuiteReportSearch(List<File> reportFolders){
		List<File> ret = new LinkedList<File>();	
		File temp = new File("");

		if(reportFolders.isEmpty()){
			return ret;
		}

		for(File folder:reportFolders){
			File[] folderContent = folder.listFiles();	//folderContent are the different folders in the evosuite-report folder. This can be csv data or subreports.

			if(folderContent == null)
				return ret;

			if(folderContent.length > 0){
				temp = new File(folder.toString() + "/statistics.csv");
				
				if(temp.exists()){
					ret.add(temp);
				}
				else if(folder.isDirectory()){
					List<File> part = recursiveEvosuiteReportSearch(Arrays.asList(folderContent));
					ret.addAll(part);
				}
			}
		}
		return ret;
	}

	private static List<File> getPathsToAllReports(File projectsFolder){
		List<File> reports = new LinkedList<File>();
		
                List<File> projects = new ArrayList<File>(Arrays.asList(projectsFolder.listFiles()));

		for(int i=0; i<projects.size();i++){
			if(projects.get(i).getName().equals("_test"))
				projects.remove(i);
		}
		
		List<File> outputFolders = getFoldersWithName(projects, "output");

		reports = ReportSearch(outputFolders);
		
		System.out.println("Found " + projects.size() + " projects");
		System.out.println("Found " + outputFolders.size() + " output folders");
		System.out.println("Found " + reports.size() + " reports");

		return reports;
	}

	private static String aggregateJacoco (File projectsFolder) throws Exception{
		
		System.out.println("aggregating jacoco coverage reports...");

		XPathFactory factory = XPathFactory.newInstance();
		XPath xPath = factory.newXPath();

		List<File> allReports = getPathsToAllReports(projectsFolder);

		String csv = "project\tsub-project\tInstruction-covered\tInstruction-missed\tBranch-covered\tBranch-missed\tLine-covered\tLine-missed\tComplexity-covered\tComplexity-missed\tMethod-covered\tMethod-missed\tClass-covered\tClass-missed";

		for(File current : allReports){

			InputSource input = new InputSource(current.toString());

			String currentLine = "\n";
			currentLine = currentLine.concat(xPath.evaluate("/report/@name", input));

			boolean isSubProject = false;

			String check = current.getParentFile().getParentFile().getParentFile().getName();	//find subproject name by folder structure
			
			currentLine = currentLine.concat("\t");

			if(check.equals("output")){
				currentLine = currentLine.concat(current.getParentFile().getName());
				isSubProject = true;
			}

			NodeList nodeList = (NodeList) xPath.compile("/report/counter").evaluate(input, XPathConstants.NODESET);

			String[] nodeTypes ={"INSTRUCTION","BRANCH","LINE","COMPLEXITY","METHOD","CLASS"};

			int classesInProject = 0;

			int j = 0;
			for(int i=0; i<nodeList.getLength(); i++){
				Node nod = nodeList.item(i);
				NamedNodeMap attributes = nod.getAttributes();
				if(!attributes.item(2).getNodeValue().equals(nodeTypes[j])){
					currentLine = currentLine.concat("\t\t");
					j++;
				}
				currentLine = currentLine.concat("\t");
				currentLine = currentLine.concat(attributes.item(0).getNodeValue());
				currentLine = currentLine.concat("\t");
				currentLine = currentLine.concat(attributes.item(1).getNodeValue());
				j++;

				//compute number of classes in current project
				if(attributes.item(2).getNodeValue().equals("CLASS")){
					classesInProject = Integer.parseInt(attributes.item(0).getNodeValue()) + Integer.parseInt(attributes.item(1).getNodeValue());
				}
			}

			csv = csv.concat (currentLine);
		}

		return csv;
	}

	private static List<File> findAllClassFiles(File parentFolder){
		List<File> current = new ArrayList<File>();

		EnumSet<ClassType> classType;

		if(parentFolder.exists()){
			File[] children = parentFolder.listFiles();

			for(File f : children){
				if (f.isDirectory())
					current.addAll(findAllClassFiles(f));
				
				else if(f.getName().endsWith(".class")){
					current.add(f);
				}
			}
		}

		return current;
	}

	private static boolean extractBinaries(String projectPath){
		if(new File(projectPath).exists()){
			try{
				String build = projectPath + "/.xcorpus/build/main";

				File buildFolder = new File(build);

				if(!buildFolder.exists()){
					buildFolder.mkdirs();
				}

				ZipFile zipFile = new ZipFile(projectPath + "/project/bin.zip");
				zipFile.extractAll(build);
				return true;
				
			}catch(ZipException e){
				System.out.println(e.toString() + " (" + projectPath + ")");
			}
		}
		return false;
	}

	private static int countClassesInProject(String projectPath, String subProjectName){
		int count = 0;

		if(new File(projectPath).exists()){
			String build = projectPath + "/.xcorpus/build/main";

			File buildFolder = new File(build);

			if(!buildFolder.exists()){
				buildFolder.mkdirs();

				if(!extractBinaries(projectPath)){
					System.out.println("something went wrong");
				}
			}

			if(!subProjectName.equals(""))
				build = build + "/" + subProjectName;

			buildFolder = new File(build);

			if(buildFolder.exists()){
				EnumSet<ClassType> classType;

				for (File f:findAllClassFiles(buildFolder)){
					try{
						classType = ClassUtil.getClassTypeOf(f);

						if(classType.contains (ClassType.CLASS) && !classType.contains(ClassType.ENUM)){
							count ++;
						}

					}catch(Exception e){						
						System.out.println(e.toString());
					}
				}
			}
		}
		return count;
	}

	private static double getEvosuiteReportCoverage(String pathToEvosuiteReport, String subProjectName){
		double ret = 0.0;
		if(pathToEvosuiteReport != null){
			File file = new File(pathToEvosuiteReport);

			if(file.exists()){
				try{
					List<String> classes = Files.readAllLines(Paths.get(pathToEvosuiteReport));
					String reportFormat = classes.get(0);
					String[] data = reportFormat.split(",");
					int coveredGoalsIndex = -1;
					int totalGoalsIndex = -1;
					for(int i=0;i<data.length;i++){
						if(data[i].equals("Covered_Goals"))
							coveredGoalsIndex = i;
						else if(data[i].equals("Total_Goals"))
							totalGoalsIndex = i;
					}

					classes.remove(0);
					String[] valuesPerClass;
					int coveredGoals = 0;
					int totalGoals = 0;

					for(int i=0; i<classes.size();i++){
						valuesPerClass = classes.get(i).split(",");
						coveredGoals += Integer.parseInt(valuesPerClass[coveredGoalsIndex]);
						totalGoals += Integer.parseInt(valuesPerClass[totalGoalsIndex]);
					}

					ret = (double)coveredGoals/(double)totalGoals;

					System.out.println(coveredGoals + " goals of " + totalGoals + " covered. coverage: " + ret);

				}catch(Exception e){
					System.out.println(e);
				}
			}
		}

		return ret;
	}

	private static String aggregateEvosuite(File projectsFolder) throws IOException{
		System.out.println("aggregating evosuite reports...");
		
                List<File> projects = new ArrayList<File>(Arrays.asList(projectsFolder.listFiles()));

		for(int i=0; i<projects.size();i++){
			if(projects.get(i).getName().equals("_test")){
				projects.remove(i);
				System.out.println("removed _test");
			}
		}

		List<File> reportFolders = getFoldersWithName(projects, "evosuite-report");

		List<File> reports = recursiveEvosuiteReportSearch(reportFolders);

		String csv = "project\tsub project\tclasses with coverage by gen. tests\tbranch coverage of classes with gen. tests (evosuite)";

		String projectName;
		String subProjectName;
		double currentCoverage;
		int currentClassesWithEvoCoverage;

		for(int i = 0; i<reports.size();i++){

			projectName = "";
			subProjectName = "";

			File parent = reports.get(i).getParentFile();
			
			if(!parent.getName().equals("evosuite-report")){
				subProjectName = parent.getName();
				parent = parent.getParentFile();
			}
			
			projectName = parent.getParentFile().getParentFile().getName();

			if(projectName.equals("_test")){
				reports.remove(i);
				continue;
			}

			System.out.print(projectName + ": ");
			currentCoverage = getEvosuiteReportCoverage(reports.get(i).toString(), subProjectName);

			
			currentClassesWithEvoCoverage = Files.readAllLines(Paths.get(reports.get(i).getPath())).size()-1;

			csv = csv.concat("\n" + projectName + "\t" + subProjectName + "\t" + currentClassesWithEvoCoverage + "\t" + currentCoverage);
		}

		System.out.println("Found " + projects.size() + " projects");
		System.out.println("Found " + reportFolders.size() + " evosuite report folders");
		System.out.println("Found " + reports.size() + " reports");

		return csv;
	}

	/* generates a report that contains information about the different class types (class, interface, enum, annotation, inner) for each project */
	private static String aggregateClassTypes(File projectsFolder){
		System.out.println("aggregating class types per project...");

                List<File> projects = Arrays.asList(projectsFolder.listFiles());

		List<File> reportFolders = getFoldersWithName(projects, "evosuite-report");

		List<File> reports = recursiveEvosuiteReportSearch(reportFolders);

		String csv = "project\tsub project\tclass files\tinterfaces\tenum\tannotations\tinner";

		System.out.print(csv);

		String projectName;
		String subProjectName;

		int ctr_classFiles;
		int ctr_interfaces;
		int ctr_enum;
		int ctr_annotations;
		int ctr_inner;

		File currentParent;
		File currentProject;
		String currentBuildFolderPath;
		File currentBuildFolder;
		List<File> currentClassFiles;
		EnumSet<ClassType> currentClassTypes;
		String currentOutputLine;


		// we need the reports to recognize subprojects
		for(File current:reports){

			projectName = "";
			subProjectName = "";
			
			ctr_classFiles = 0;
			ctr_interfaces = 0;
			ctr_enum = 0;
			ctr_annotations = 0;
			ctr_inner = 0;

			currentParent = current.getParentFile();
			
			if(!currentParent.getName().equals("evosuite-report")){
				subProjectName = currentParent.getName();
				currentParent = currentParent.getParentFile();
			}
			
			currentProject = currentParent.getParentFile().getParentFile();

			projectName = currentProject.getName();
			
			if(projectName.equals("_test"))
				continue;

			extractBinaries(currentProject.toString());

			currentBuildFolderPath = currentProject.toString() + "/.xcorpus/build/main";

			if(!subProjectName.equals(""))
				currentBuildFolderPath = currentBuildFolderPath + "/" + subProjectName;

			currentBuildFolder = new File(currentBuildFolderPath);

			if(currentBuildFolder.exists()){

				currentClassFiles = findAllClassFiles(currentBuildFolder);
				
				for(File classFile:currentClassFiles){
					try{
						currentClassTypes = ClassUtil.getClassTypeOf(classFile);

						if(currentClassTypes.contains(ClassType.CLASS))
							ctr_classFiles ++;
					
						if(currentClassTypes.contains(ClassType.INTERFACE))
							ctr_interfaces ++;
					
						if(currentClassTypes.contains(ClassType.ENUM))
							ctr_enum ++;
					
						if(currentClassTypes.contains(ClassType.ANNOTATION))
							ctr_annotations ++;

						if(classFile.getName().contains("$"))
							ctr_inner ++;
						}
					catch(Exception e){
						System.out.println(e.toString());
					}
				}
			}
			else {
				System.out.println("\nno build folder found at " + currentBuildFolderPath);

				continue;
			}
			
			currentOutputLine = "\n" + projectName + "\t" + subProjectName + "\t" + ctr_classFiles + "\t" + ctr_interfaces + "\t" + ctr_enum + "\t" + ctr_annotations + "\t" + ctr_inner;
			
			System.out.print(currentOutputLine);

			csv = csv.concat(currentOutputLine);
		}
		return csv;
	}

	//helper function to update a line in a given report with the given data. Only empty values will be overwritten by the new ones. Works only iff the length of the current line and the one of the new data are the same. Adds a new line at the end if the given data does not match any project in the report yet.
	private static List<String[]> reportListUpdateHelper(List<String[]> report, String[] data){

		boolean update = false;

		if(report.size()>0){

			for(int i=0;i<report.size();i++){
				if(report.get(i)[0].equals(data[0]) && report.get(i)[1].equals(data[1])){
					if(report.get(i).length == data.length){
						String[] line = report.get(i);
					
						//update line with information of data[]
						for(int j=0;j<line.length;j++){
							if(line[j].equals("") && !data[j].equals("")){
								line[j] = data[j];
								update = true;
							}
						}

						// update only first occurrence of the same projectName-subProjectName pair since this should be the "primary key"
						if(update == true){
							report.remove(i);
							report.add(i,line);
							break;
						}
					}
					else{
						System.out.println("data format mismatch");
						break;
					}
				}
			}
		}

		if(!update){
			if(report.get(0).length == data.length)
				report.add(data);
			else {
				System.out.print("data format mismatch: report length is " + report.get(0).length);
				System.out.println("");			
			}
		}
		return report;
	}

	private static String createMainReport(){
		String ret = "";
		List<String[]> retAsList = new LinkedList<String[]>();

		File classTypes = new File("classtypes.csv");
		File evosuite = new File("evosuite-report.csv");
		File builtinTests = new File("jacoco-report-builtin-tests.csv");
		File generatedTests = new File("jacoco-report-generated-tests.csv");
		File allTests = new File("jacoco-report-all-tests.csv");

		
		if(classTypes.exists() && evosuite.exists() && builtinTests.exists() && generatedTests.exists() && allTests.exists()){

			String project = "";
			String subProject = "";
			String classFiles = "";
			String builtinTestBranchCoverage = "";
			String classesWithCoverageByGenTests = "";
			String branchCoverageOfClassesWithGenTests = "";
			String GenTestBranchCoverage = "";
			String TotalTestBranchCoverage = "";

			String[] reportLine = {"project","sub project","class files","built-in test branch coverage (jacoco)","classes with coverage by gen. tests","branch coverage of classes with gen. tests (evosuite)","gen. test branch coverage (jacoco)","total test branch coverage (jacoco)"};

			retAsList.add(reportLine);

			try{
				System.out.println("adding data of classtypes.csv");
				//add data of classtypes.csv
				for(String line : Files.readAllLines(classTypes.toPath())){
					reportLine = line.split("\t");
					
					//ignore first line of report
					if(reportLine[0].equals("project"))
						continue;

					String[] data = {reportLine[0],reportLine[1],reportLine[2],"","","","",""};

					retAsList = reportListUpdateHelper(retAsList, data);
				}

				System.out.println("adding data of jacoco-report-builtin-tests.csv");
				//add data of jacoco report for builtin tests
				for(String line : Files.readAllLines(builtinTests.toPath())){
							
					reportLine = line.split("\t");
					
					//ignore first line (headline) of report
					if(reportLine[0].equals("project"))
						continue;

					double builtinCoverage = Double.parseDouble(reportLine[4])/(Double.parseDouble(reportLine[4])+Double.parseDouble(reportLine[5]));

					String[] data = {reportLine[0],reportLine[1],"",Double.toString(builtinCoverage),"","","",""};

					retAsList = reportListUpdateHelper(retAsList, data);
				}

				System.out.println("adding data of evosuite-report.csv");
				//add data of evosuite report 
				for(String line : Files.readAllLines(evosuite.toPath())){
					
					reportLine = line.split("\t");

					//ignore first line (headline) of report
					if(reportLine[0].equals("project"))
						continue;

					String[] data = {reportLine[0],reportLine[1],"","",reportLine[2],reportLine[3],"",""};

					retAsList = reportListUpdateHelper(retAsList, data);					
				}

				System.out.println("adding data of jacoco-report-generated-tests.csv");
				//add data of jacoco report for generated tests
				for(String line : Files.readAllLines(generatedTests.toPath())){
					
					reportLine = line.split("\t");

					//ignore first line (headline) of report
					if(reportLine[0].equals("project"))
						continue;

					double evosuiteCoverage = Double.parseDouble(reportLine[4])/(Double.parseDouble(reportLine[4])+Double.parseDouble(reportLine[5]));

					String[] data = {reportLine[0],reportLine[1],"","","","",Double.toString(evosuiteCoverage),""};

					retAsList = reportListUpdateHelper(retAsList, data);
				}

				System.out.println("adding data of jacoco-report-all-tests.csv");
				//add data of jacoco report for all tests
				for(String line : Files.readAllLines(generatedTests.toPath())){
					
					reportLine = line.split("\t");

					//ignore first line (headline) of report
					if(reportLine[0].equals("project"))
						continue;

					double evosuiteCoverage = Double.parseDouble(reportLine[4])/(Double.parseDouble(reportLine[4])+Double.parseDouble(reportLine[5]));

					String[] data = {reportLine[0],reportLine[1],"","","","","",Double.toString(evosuiteCoverage)};

					retAsList = reportListUpdateHelper(retAsList, data);
				}


				//add retAsList data to the ret string
				for(String[] line : retAsList){
					for(String s : line){
						ret = ret.concat(s);
						ret = ret.concat("\t");
					}
					ret = ret.concat("\n");
				}

			}catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			System.out.println("one or more of the neccessary reports are missing");
		}

		return ret;
	}

	/*
		Runs the ReportAggregator for xcorpus. 
		args[0]		- parent folder of the projects.
		args[1]		- specify which kind of report to aggregate. options:
			classtypes 		- analyse project binaries (counts different class types per project)
			evosuite		- analyse evosuite reports (contains only coverage data for classes evosuite was able to compute)
			builtin-tests		- analyse runtime coverage data for built-in tests
			generated-tests		- analyse runtime coverage data for generated tests (for the whole project, in contrast to the evosuite option)
			all-tests		- analyse runtime coverage data for all tests (built-in and generated)
	*/
	public static void execute(String[] args){
		
                File projects = new File(args[0]);

		if(args.length>0){
			try{
				if(args[1].equals("classtypes")){
					String data = aggregateClassTypes(projects);
					String reportName = "classtypes.csv";
					PrintWriter out = new PrintWriter(reportName);
					out.println(data);
					out.close();
				}
				else if(args[1].equals("evosuite")){
					String data = aggregateEvosuite(projects);
					String reportName = "evosuite-report.csv";
					PrintWriter out = new PrintWriter(reportName);
					out.println(data);
					out.close();
				}
				else if((args[1].equals("builtin-tests"))|
					(args[1].equals("generated-tests"))|
					(args[1].equals("all-tests"))){

					String data = aggregateJacoco(projects);
					String reportName = "jacoco-report-" + args[1] +".csv";
					PrintWriter out = new PrintWriter(reportName);
					out.println(data);
					out.close();
				}else if(args[1].equals("dacapo")){
					String data = aggregateJacoco(projects);
					String reportName = "dacapo-report-" + args[1] +".csv";
					PrintWriter out = new PrintWriter(reportName);
					out.println(data);
					out.close();
				}else if(args[1].equals("xcorpus-main")){
					String data = createMainReport();
					String reportName = args[1] + ".csv";
					PrintWriter out = new PrintWriter(reportName);
					out.println(data);
					out.close();
				}
				else 
					System.out.println("no report type specified");
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		else System.out.println("no report type specified");

	}
}
