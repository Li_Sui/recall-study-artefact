package xcorpus;

import java.io.*;
import org.jdom2.*;


public class TestSuiteModificator{
        public static void main(String[] args){
                iteratorStarter(args);   
        }

        public static void iteratorStarter(String[] args){
		String pathToProject = "";

                if(args.length>1){
                        File projects = new File(args[0]);
			if(projects.exists()){
	                        File[] list = projects.listFiles();
        	                boolean projectFound=false;
        	                for(int i=0;i<list.length;i++){
        	                        if(list[i].getName().contains(args[1])){
        	                                projectFound=true;
        	                                System.out.println("found project " + list[i].getName());
						pathToProject = args[0] + list[i].getName() + "/.xcorpus/evosuite-tests";
        	                                recursiveTestIterator(pathToProject);
        	                        }			
	                        }
	                        if(!projectFound)
	                                System.out.println("could not find applicable project for " + args[0]);
			}
			else{
				System.out.println("could not find applicable project at " + pathToProject);
			}
                }
                        

                else if(args.length==0)
                        recursiveTestIterator();    
        }

        /*
        * iterates recursively over all existing generated test suites
        */
        private static void recursiveTestIterator(){
                recursiveTestIterator("../data/qualitas_corpus_20130901");
        }

        private static File directory;
        private static File[] list;

        /*
        * iterates over the generated tests in a specific project or folder
        */
        private static void recursiveTestIterator(String folderName){
                directory = new File(folderName);

                if(directory != null){
                        list = directory.listFiles();

                        if(list != null){
                                for(File child: list){

                                        if(child.isDirectory()){
                                                recursiveTestIterator(child.toString());
                                        }

                                        else{
                                                if(child.getName().endsWith("_ESTest.java")){

                                                        if(!deleteIfEmpty(child.toString()))
                                                                replaceImports(child.toString());       //try imports replacement only if the test was not deleted before
                                                }

                                        }                                        
                                }
                        }
                }
        }

        private static File testIfEmptyFile;
        private static File secondTestIfEmptyFile;
        /*
        * Deletes test file if it contains no test methods (-> smaller than 600 bytes)
        * and returns true or false respectively
        */
        private static boolean deleteIfEmpty(String pathToTest){
                if(!pathToTest.endsWith("_ESTest.java"))
                        return false;
                else{
                        testIfEmptyFile = new File(pathToTest);
                        if(testIfEmptyFile.length()<600){                                
                                System.out.println("trying to delete empty test "+ testIfEmptyFile.getName());
                                
                                String scaff = pathToTest.replace(".java", "_scaffolding.java");
                                
                                secondTestIfEmptyFile = new File(scaff);

                                if(testIfEmptyFile.delete() && secondTestIfEmptyFile.delete()){
                                       System.out.println("success");
                                       return true;
                                }
                                else {
                                        System.out.println("error while deleting " + testIfEmptyFile.getName() + " or " + secondTestIfEmptyFile.getName());
                                        return false;
                                }                                
                        }
                        else return false;
                }
        }


        private static File replaceFile;
        private static String tempLine;
        private static String tempOldText;
        private static String tempNewText;
        private static BufferedReader tempReader;
        private static FileWriter tempWriter;

        /*
        * replaces the imports in order to activate our assertion filter xcorpus.AssertionAdapter
        */
        private static void replaceImports(String pathToTest){
                //System.out.println(pathToTest);

         try{
                replaceFile = new File(pathToTest);
                tempReader = new BufferedReader(new FileReader(replaceFile));
                tempLine = "";
                tempOldText = "";

                while((tempLine = tempReader.readLine()) != null){
                        tempOldText += tempLine + "\r\n";
                }
                tempReader.close();
             
                tempNewText = tempOldText.replaceAll("import static org.junit.Assert.*;", "import static xcorpus.AssertionAdapter.*;");
                tempNewText = tempNewText.replaceAll("import static org.evosuite.runtime.EvoAssertions.*;", "");
                
                if(!tempNewText.equals(tempOldText)){
                        System.out.print("changed File ");
                        System.out.println(pathToTest);
                }
                else {
                        System.out.print("nothing changed in ");
                        System.out.println(pathToTest);
                }

                tempWriter = new FileWriter(replaceFile.toString());
                tempWriter.write(tempNewText);
                tempWriter.close();
         }
         catch (IOException ioe){

             ioe.printStackTrace();
         }

        }
}
