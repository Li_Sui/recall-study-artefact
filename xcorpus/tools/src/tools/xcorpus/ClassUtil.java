package xcorpus;

import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.EnumSet;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.ClassReader;


/**
 * Utility to assign types to binary class files.
 * @author jens dietrich
 */
public class ClassUtil{  

	private static class ClassTypeVisitor extends ClassVisitor {
		EnumSet<ClassType> types = EnumSet.noneOf(ClassType.class);
		ClassTypeVisitor() {
			super(Opcodes.ASM5);
		}
		@Override
	    public void visit(final int version, final int access, final String name, final String signature, final String superName, final String[] interfaces) {
	        if (name.contains("$")) {
	        	this.types.add(ClassType.INNER);
	        }
	        if (checkFlag(access,Opcodes.ACC_ABSTRACT )) {
	        	this.types.add(ClassType.ABSTRACT);
	        }

	        if (checkFlag(access,Opcodes.ACC_INTERFACE )) {
	        	this.types.add(ClassType.INTERFACE);
	        }
	        else if (checkFlag(access,Opcodes.ACC_ENUM )) {
	        	this.types.add(ClassType.ENUM);
	        }
	        else if (checkFlag(access,Opcodes.ACC_ANNOTATION  )) {
	        	this.types.add(ClassType.ANNOTATION);
	        }
	        else {
	        	this.types.add(ClassType.CLASS);
	        }
	    }

	    private boolean checkFlag (int flag,int opcode) {
    		return (opcode & flag) == opcode;
    	}

	}

	public static EnumSet<ClassType> getClassTypeOf (File file) throws Exception {
		ClassTypeVisitor visitor = new ClassTypeVisitor();
		try (InputStream in = new FileInputStream(file)) {
			new ClassReader(in).accept(visitor, 0);
		}
		return visitor.types;
	}

}
