#!/bin/bash

# import properties
# ${XCORPUS_DATA_DIR}=where the xcorpus is
# ${RECALL_ROOT_DIR} =artefact working directory
# ${programs}=list of programs
import_property(){
	source "${RECALL_ROOT_DIR}"/property.sh
}

produce_stats(){
  check_prerequisite "$1"
  echo "[recall-artefact] Generating stats: java parameters are set to -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize}"
  #you can change java parameters in property.sh
  input=${RECALL_ROOT_DIR}/data/processed-CCTs-data
  output=${RECALL_ROOT_DIR}/data/statsAndGraphs
  #produce intermediate results for RQ4 (cause analysis), results will be added to ${RECALL_ROOT_DIR}/data/processed-CCTs-data
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.ProduceTagedFNs -inputDir ${input}"
  #RQ1
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.RQ1Spreadsheet -inputDir ${input} -outputDir ${output}"
  #RQ2
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.RQ2Spreadsheet -inputDir ${input} -outputDir ${output}"
  #RQ3
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.RQ3Spreadsheet -inputDir ${input} -outputDir ${output}"
  #RQ4 boxplot+ detailed analysis
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.AnalyseFNCategory -inputDir ${input} -outputDir ${output}"
  #RQ4. Sunburst. remove sunburst figure as it needs to be convert to Microsoft Spreadsheet format
  eval "java -Xmx${rawCCTheapMem} -Xss${rawCCTStackSize} -cp ${RECALL_ROOT_DIR}/libs/CCT-1.0-result-analysis.jar nz.ac.massey.cs.resultAnalysis.RQ4Sunburst -inputDir ${input} -outputDir ${output}"
}

produce_graphs(){
   echo "[recall-artefact] Generating graphs using R"
   eval "Rscript ${RECALL_ROOT_DIR}/produceStats_script/graphs.r ${RECALL_ROOT_DIR}/data/statsAndGraphs"
}

#$1=program
check_prerequisite(){
  if [[ ! -d "${RECALL_ROOT_DIR}/data/processed-CCTs-data/$1" ]] || [[ ! -d "${RECALL_ROOT_DIR}/data/doop-results/lib-setup/$1" ]]
  then
      echo "[recall-artefact] processed CCTs must exist!, Doop SCGs must exist"
      exit 1
  fi
}

main (){
  import_property
  if [ -z "$1" ]
  then
    produce_stats
    produce_graphs
  else
    echo "[recall-artefact] this script does not require parameters"
  fi
}

main $@
