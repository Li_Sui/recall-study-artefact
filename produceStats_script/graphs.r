#author amjed tahir (a.tahir@massey.ac.nz)

#install.packages("vioplot")
library(vioplot)

# get current working directory where r script is located in the repo
args = commandArgs(trailingOnly=TRUE)
if (length(args)!=1) {
  stop("one argument must be supplied (working directory).n", call.=FALSE)
}
path <- args[1]

recall_builtin_generated <- read.csv(file = file.path(path,"RQ1.csv"))
recall_base_contextsensitive <- read.csv(file = file.path(path,"RQ2.csv"))
recall_base_reflection <- read.csv(file = file.path(path,"RQ3.csv"))
#
categorised_FNS_base <- read.csv(file = file.path(path,"RQ4-boxplot-contextInsensitive.csv"))
categorised_FNS_reflection<- read.csv(file = file.path(path,"RQ4-boxplot-reflection.csv"))

#FNratio_uncategorised <- read.csv(file = file.path(path,"uncategorised.csv"))


#RQ1: Recall of the base static analysis with respect to oracles for both lib and superjar mode
png(filename=file.path(path,"recall-builtin-vs-generated.png"), width = 700, height = 400)
vioplot(recall_builtin_generated$builtin.lib,recall_builtin_generated$builtin.super,recall_builtin_generated$generated.lib,recall_builtin_generated$generated.super,recall_builtin_generated$combined.lib,recall_builtin_generated$combined.super,
        names = c("builtin lib", "builtin super","generated lib", "generated super","combined lib", "combined super"), col = "gray")
dev.off()

#RQ2 recall of context-sensitive vs context-insensitive analysis
png(filename=file.path(path,"recall-base-vs-contextsensitive.png"), width = 750, height = 400)
vioplot(recall_base_contextsensitive$combined.base,recall_base_contextsensitive$combined.contextsensitive,
       names = c("combined base", "combined contextsensitive"), col = "gray")
dev.off()


#RQ3 context-insensitive analysis vs context-sensitive  vs reflection analysis(combined)
png(filename=file.path(path,"recall-base-vs-reflection_combined.png"), width = 650, height = 400)
vioplot(recall_base_contextsensitive$combined.base,recall_base_contextsensitive$combined.contextsensitive,recall_base_reflection$combined.ref,
        names = c("base", "contextsensitive","ref (20)"), ylab = "recall",col = "gray")
dev.off()


##RQ4: false negatives in the base static analysis
png(filename=file.path(path,"categorised_FNS_base.png"), width = 750, height = 400)
vioplot(categorised_FNS_base$DI,categorised_FNS_base$DALL,categorised_FNS_base$DACC,categorised_FNS_base$SYS,categorised_FNS_base$other,
        names = c("DI", "DALL","DACC","SYS", "Other"), ylab = "percentage", xlab = "category", col = "gray")
dev.off()

#RQ4:false negatives in the static analysis with reflection support
png(filename=file.path(path,"categorised_FNS_reflection.png"), width = 750, height = 400)
vioplot(categorised_FNS_reflection$DI,categorised_FNS_reflection$DALL,categorised_FNS_reflection$DACC,categorised_FNS_reflection$SYS,categorised_FNS_reflection$other,
        names = c("DI", "DALL","DACC","SYS", "Other"), ylab = "percentage", xlab = "category", col = "gray")
dev.off()